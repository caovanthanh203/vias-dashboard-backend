// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import testData from "./assets/js/data.json"

Vue.config.productionTip = false


require("select2/dist/css/select2.min.css");
require("./assets/plugin/jstree/style.css");
require("./assets/plugin/nestable/jquery.nestable.css");

require("./assets/css/bootstrap.min.css");
require("./assets/css/icons.css")
require("./assets/css/style.css")
require("./assets/css/vias-custom.css")

window.$ = window.jQuery = require("jquery");
require("popper.js");
require("bootstrap");
require("./assets/js/detect");
require("./assets/js/fastclick");
require("jquery-slimscroll");
require("block-ui");
require("./assets/js/waves");
window.WOW = require("wow.js");
require("jquery.nicescroll");
require("jquery.scrollto");
require("select2");
require("chart.js");
require("jstree");
require("peity");
require("nestable");
window.zingchart = require("zingchart");

require("./assets/js/jquery.core");

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  data() {
    return {
      myData: testData,
    }
  },
  mounted() {
    $("body").append('<script src="./static/jquery.app.js"></script>');
  }
})
