import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/dashboard/dashboard'
import LineView from '@/components/dashboard/line'
import ChartView from '@/components/dashboard/chart'
import SpeedView from '@/components/dashboard/speed'
import Employees from '@/components/employees/list'
import Department from '@/components/department/list'
import Kpi from '@/components/kpi/list'
import Report from '@/components/report/report'
import ReportList from '@/components/report/list'
import ReportImport from '@/components/report/import'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Dashboard,
      name: Dashboard.name,
      icon: Dashboard.icon,
      children: [
        {
          path: "line-view",
          component: LineView,
          name: LineView.name,
          viewName: LineView.viewName,
          icon: LineView.icon,
          bg: LineView.bg
        },
        {
          path: "chart-view",
          component: ChartView,
          name: ChartView.name,
          viewName: ChartView.viewName,
          icon: ChartView.icon,
          bg: ChartView.bg
        }
        ,
        {
          path: "speed-view",
          component: SpeedView,
          name: SpeedView.name,
          viewName: SpeedView.viewName,
          icon: SpeedView.icon,
          bg: SpeedView.bg
        }
      ]
    },
    {
      path: '/report',
      component: Report,
      name: Report.name,
      icon: ReportList.icon,
      children: [
        {
          path: "list",
          component: ReportList,
          name: ReportList.name,
        },
        {
          path: "import",
          component: ReportImport,
          name: ReportList.name,

        }
      ]
    },
    {
      path: '/kpi',
      component: Kpi,
      name: Kpi.name,
      icon: Kpi.icon
    },
    {
      path: '/employees',
      component: Employees,
      name: Employees.name,
      icon: Employees.icon
    },
    {
      path: '/deparment',
      component: Department,
      name: Department.name,
      icon: Department.icon
    }
  ]
})
