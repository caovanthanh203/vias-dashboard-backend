<?php

namespace App\Http\Controllers;

use App\Models\Kpi\KpiRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller {

	private $model;

	public function __construct(KpiRepository $model) {
		$this->authCheck();
		$this->model = $model;
	}

	public function settingStore(Request $request) {
		$init = 0;
		if ($request->has('init')) {
			$init = $request->init;
		};
		$kpi_ids = $request->kpi;
		if ($user = auth()->user()) {
			$user->home_kpi = collect($kpi_ids)->toArray();
			$user->init = $init;
			$user->save();
		}
		return $this->handleSuccess('Cập nhật cài đặt thành công!');
	}

	public function settingList(Request $request) {
		$kpi_ids = [];
		$init = false;
		if ($user = auth()->user()) {
			$kpi_ids = $user->home_kpi;
			if (!is_array($kpi_ids)) {
				$kpi_ids = [];
			};
			$init = $user->init;
		}
		return ['kpi' => $kpi_ids, 'init' => $init];
	}

	public function data() {
		if ($user = auth()->user()) {
			$time = "(Q" . Carbon::now()->quarter . ", " . Carbon::now()->format('m-Y') . ")";
			$company_id = $user->company_id;
			$kpi_ids = $user->home_kpi;
			if (!is_array($kpi_ids)) {
				$kpi_ids = [];
			};
			$repo = clone $this->model;
			$data = [];
			$kpis = $repo
				->scopeQuery(function ($kpi) use ($kpi_ids, $company_id) {
					return $kpi->where('company_id', $company_id)->whereHas('targets')->whereIn('id', $kpi_ids);
				})
				->setTransform(function ($item) {
					return [
						'id' => $item->id,
						'type' => $item->charttype->name,
					];
				})->paginate();
			foreach ($kpis as $kpi) {
				switch ($kpi['type']) {
				case 'type-bar':
					$data[] = $this->bar($kpi['id']);
					break;
				case 'type-bar-stacked':
					$data[] = $this->stacked($kpi['id']);
					break;
				case 'type-line':
					$data[] = $this->line($kpi['id']);
					break;
				case 'type-pie':
					$data[] = $this->pie($kpi['id']);
					break;
				case 'type-combo':
					$data[] = $this->combo($kpi['id']);
					break;
				default:
					$data[] = $this->rada($kpi['id']);
					break;
				}
			}
			// $data['time'] = $time;
			// $bar = $this->bar();
			// $stacked = $this->stacked();
			// $line = $this->line();
			// $pie = $this->pie();
			// $combo = $this->combo();
			return ['data' => $data, 'time' => $time];
		}
	}

	private function bar($id_x) {
		$color = collect([
			"#6666FF", "#E6331A",
			"#B3B31A", "#00E680",
			"#4D8066", "#66991A",
			"#6680B3", "#66994D",
			"#E666B3", "#FFB399",
			"#FF6633", "#B366CC",
			"#B33300", "#80B300",
			"#4D80CC", "#00B3E6",
			"#FF99E6", "#FF1A66",
			"#991AFF", "#FF33FF",
			"#4DB3FF", "#B34D4D",
			"#66664D", "#4DB380",
			"#E6B3B3", "#E64D66",
		]);
		// if ($id = $request->id){
		$id = $id_x;
		$kpi = $this->model->find($id);
		$label = $kpi->getDataLabel();
		$times = 1;
		$average_data = 0;
		$current_data = 0;
		$current_target = $kpi->current_time_target;
		if ($kpi->is_month) {
			$times = Carbon::now()->month;
		} else {
			$times = Carbon::now()->quarter;
		}
		$data = $kpi->targets->transform(function ($item) use ($kpi, $color, $times) {
			$data = $item->getData($kpi->id, $kpi->time_frame);
			$time_data = $data[$times - 1];
			$average_data = collect($data)->sum();
			return [
				'label' => $item->name,
				'data' => $data,
				'average_data' => $average_data,
				'time_data' => $time_data,
				"backgroundColor" => $color->random(),
				"borderWidth" => 0,
			];
		});
		$average_data = round($data->sum('average_data') / $times, 2);
		$current_data = $data->sum('time_data');
		$percent = 0;
		$average_target = round($kpi->getSumTarget() / $times, 2);
		$percent = $this->divide($average_data, $average_target);
		$current_percent = $this->divide($current_data, $current_target);
		$struct = [
			"id" => 'bar-' . $kpi->code,
			"name" => $kpi->name,
			'average_target' => $average_target,
			'percent' => $percent,
			'percent_text' => $this->getLabel($percent),
			'current_data' => $current_data,
			'current_target' => $current_target,
			'current_percent' => $current_percent,
			'current_percent_text' => $this->getLabel($current_percent),
			'average_data' => $average_data,
			'chart' => [
				"type" => "bar",
				"label" => $label,
				"objects" => $data,
			],
		];
		return $struct;
		// $data_department = $kpi->targets->transform(function ($item) use ($kpi){
		//     return [
		//     'id'     => $item->id,
		//     'object' => $item->name,
		//     'data'   => $item->getData($kpi->id, $kpi->time_frame)
		//     ];
		// });
		// return [
		// 'id' => $id,
		// 'rows_user' => $data_user,
		// 'rows_department' => $data_department,
		// 'target' => $target,
		// 'label' => $label
		// ];
		//}
	}

	private function stacked($id_x) {
		$color = collect([
			"#6666FF", "#E6331A",
			"#B3B31A", "#00E680",
			"#4D8066", "#66991A",
			"#6680B3", "#66994D",
			"#E666B3", "#FFB399",
			"#FF6633", "#B366CC",
			"#B33300", "#80B300",
			"#4D80CC", "#00B3E6",
			"#FF99E6", "#FF1A66",
			"#991AFF", "#FF33FF",
			"#4DB3FF", "#B34D4D",
			"#66664D", "#4DB380",
			"#E6B3B3", "#E64D66",
		]);
		// if ($id = $request->id){
		$id = $id_x;
		$kpi = $this->model->find($id);
		$label = $kpi->getDataLabel();
		$times = 1;
		$average_data = 0;
		$current_data = 0;
		$current_target = $kpi->current_time_target;
		if ($kpi->is_month) {
			$times = Carbon::now()->month;
		} else {
			$times = Carbon::now()->quarter;
		}
		$data = $kpi->targets->transform(function ($item) use ($kpi, $color, $times) {
			$data = $item->getData($kpi->id, $kpi->time_frame);
			$time_data = $data[$times - 1];
			$average_data = collect($data)->sum();
			return [
				'label' => $item->name,
				'data' => $data,
				'average_data' => $average_data,
				'time_data' => $time_data,
				"backgroundColor" => $color->random(),
				"borderWidth" => 0,
			];
		});
		$average_data = round($data->sum('average_data') / $times, 2);
		$current_data = $data->sum('time_data');
		$percent = 0;
		$average_target = round($kpi->getSumTarget() / $times, 2);
		$percent = $this->divide($average_data, $average_target);
		$current_percent = $this->divide($current_data, $current_target);
		$struct = [
			"id" => 'st-bar-' . $kpi->code,
			"name" => $kpi->name,
			'average_target' => $average_target,
			'percent' => $percent,
			'percent_text' => $this->getLabel($percent),
			'current_data' => $current_data,
			'current_target' => $current_target,
			'current_percent' => $current_percent,
			'current_percent_text' => $this->getLabel($current_percent),
			'average_data' => $average_data,
			'chart' => [
				"type" => "bar-stacked",
				"label" => $label,
				"objects" => $data,
			],
		];
		return $struct;
	}

	private function line($id_x) {
		$color = collect([
			"#6666FF", "#E6331A",
			"#B3B31A", "#00E680",
			"#4D8066", "#66991A",
			"#6680B3", "#66994D",
			"#E666B3", "#FFB399",
			"#FF6633", "#B366CC",
			"#B33300", "#80B300",
			"#4D80CC", "#00B3E6",
			"#FF99E6", "#FF1A66",
			"#991AFF", "#FF33FF",
			"#4DB3FF", "#B34D4D",
			"#66664D", "#4DB380",
			"#E6B3B3", "#E64D66",
		]);
		// if ($id = $request->id){
		$id = $id_x;
		$kpi = $this->model->find($id);
		$label = $kpi->getDataLabel();
		$times = 1;
		$average_data = 0;
		$current_data = 0;
		$current_target = $kpi->current_time_target;
		if ($kpi->is_month) {
			$times = Carbon::now()->month;
		} else {
			$times = Carbon::now()->quarter;
		}
		$data = $kpi->targets->transform(function ($item) use ($kpi, $color, $times) {
			$c = $color->random();
			$data = $item->getData($kpi->id, $kpi->time_frame);
			$time_data = $data[$times - 1];
			$average_data = collect($data)->sum();
			return [
				'label' => $item->name,
				'data' => $data,
				'average_data' => $average_data,
				'time_data' => $time_data,
				"pointBackgroundColor" => $c,
				"borderColor" => $c,
				"backgroundColor" => "transparent",
			];
		});
		$average_data = round($data->sum('average_data') / $times, 2);
		$current_data = $data->sum('time_data');
		$percent = 0;
		$average_target = round($kpi->getSumTarget() / $times, 2);
		$percent = $this->divide($average_data, $average_target);
		$current_percent = $this->divide($current_data, $current_target);
		$struct = [
			"id" => 'line-' . $kpi->code,
			"name" => $kpi->name,
			'average_target' => $average_target,
			'percent' => $percent,
			'percent_text' => $this->getLabel($percent),
			'current_data' => $current_data,
			'current_target' => $current_target,
			'current_percent' => $current_percent,
			'current_percent_text' => $this->getLabel($current_percent),
			'average_data' => $average_data,
			'chart' => [
				"type" => "line",
				"label" => $label,
				"objects" => $data,
			],
		];
		return $struct;
	}

	private function pie($id_x) {
		$color = collect([
			"#6666FF", "#E6331A",
			"#B3B31A", "#00E680",
			"#4D8066", "#66991A",
			"#6680B3", "#66994D",
			"#E666B3", "#FFB399",
			"#FF6633", "#B366CC",
			"#B33300", "#80B300",
			"#4D80CC", "#00B3E6",
			"#FF99E6", "#FF1A66",
			"#991AFF", "#FF33FF",
			"#4DB3FF", "#B34D4D",
			"#66664D", "#4DB380",
			"#E6B3B3", "#E64D66",
		]);
		// if ($id = $request->id){
		$id = $id_x;
		$kpi = $this->model->find($id);
		$label = $kpi->getDataLabel();
		$times = 1;
		$average_data = 0;
		$current_data = 0;
		$current_target = $kpi->current_time_target;
		if ($kpi->is_month) {
			$times = Carbon::now()->month;
		} else {
			$times = Carbon::now()->quarter;
		}
		$data = $kpi->targets->transform(function ($item) use ($kpi, $color, $times) {
			$c = $color->random();
			$data = $item->getData($kpi->id, $kpi->time_frame);
			$time_data = $data[$times - 1];
			$average_data = collect($data)->sum();
			return [
				'label' => $item->name,
				'data' => $average_data,
				'time_data' => $time_data,
				"pointBackgroundColor" => $c,
			];
		});
		$label = $data->pluck('label')->toArray();
		$average_data = $data->sum('data') / $times;
		$current_data = $data->sum('time_data');
		$ob_data = $data->pluck('data');
		$ob_data = $ob_data->toArray();
		$ob_color = $data->pluck('pointBackgroundColor')->toArray();
		$percent = 0;
		$average_target = round($kpi->getSumTarget() / $times, 2);
		$percent = $this->divide($average_data, $average_target);
		$current_percent = $this->divide($current_data, $current_target);
		$struct = [
			"id" => 'pie-' . $kpi->code,
			"name" => $kpi->name,
			'average_target' => $average_target,
			'percent' => $percent,
			'percent_text' => $this->getLabel($percent),
			'current_data' => $current_data,
			'current_target' => $current_target,
			'current_percent' => $current_percent,
			'current_percent_text' => $this->getLabel($current_percent),
			'average_data' => $average_data,
			'chart' => [
				"type" => "pie",
				"label" => $label,
				"object" => [
					"data" => $ob_data,
					"backgroundColor" => $ob_color,
					"borderWidth" => 0,
				],
			],
		];
		return $struct;
	}

	private function combo($id_x) {
		$color = collect([
			"#6666FF", "#E6331A",
			"#B3B31A", "#00E680",
			"#4D8066", "#66991A",
			"#6680B3", "#66994D",
			"#E666B3", "#FFB399",
			"#FF6633", "#B366CC",
			"#B33300", "#80B300",
			"#4D80CC", "#00B3E6",
			"#FF99E6", "#FF1A66",
			"#991AFF", "#FF33FF",
			"#4DB3FF", "#B34D4D",
			"#66664D", "#4DB380",
			"#E6B3B3", "#E64D66",
		]);
		// if ($id = $request->id){
		$id = $id_x;
		$kpi = $this->model->find($id);
		$label = $kpi->getDataLabel();
		$department = $kpi->targets->first();
		$data = $department->getData($kpi->id, $kpi->time_frame);
		$times = 1;
		$average_data = 0;
		$current_data = 0;
		$current_target = $kpi->current_time_target;
		$average_data = collect($data)->sum() / $times;
		if ($kpi->is_month) {
			$time_data = $data[Carbon::now()->month - 1];
		} else {
			$time_data = $data[Carbon::now()->quarter - 1];
		}
		$percent = 0;
		$average_target = round($kpi->getSumTarget() / $times, 2);
		$percent = $this->divide($average_data, $average_target);
		$current_percent = $this->divide($current_data, $current_target);
		$real = [
			'label' => $department->name,
			'data' => $data,
			"backgroundColor" => $color->random(),
		];
		$target = [
			"label" => "target",
			"type" => "line",
			'data' => $kpi->getDataTarget(),
			"pointBackgroundColor" => $color->random(),
			"borderColor" => $color->random(),
			"backgroundColor" => "transparent",
		];
		$struct = [
			"id" => 'combo-' . $kpi->code,
			"name" => $kpi->name,
			'average_target' => $average_target,
			'percent' => $percent,
			'percent_text' => $this->getLabel($percent),
			'current_data' => $current_data,
			'current_target' => $current_target,
			'current_percent' => $current_percent,
			'current_percent_text' => $this->getLabel($current_percent),
			'average_data' => $average_data,
			'chart' => [
				"type" => "combo",
				"label" => $label,
				"object_1" => $real,
				"object_2" => $target,
			],
		];
		return $struct;
	}

	private function rada($id_x) {
		$color = collect([
			"#6666FF", "#E6331A",
			"#B3B31A", "#00E680",
			"#4D8066", "#66991A",
			"#6680B3", "#66994D",
			"#E666B3", "#FFB399",
			"#FF6633", "#B366CC",
			"#B33300", "#80B300",
			"#4D80CC", "#00B3E6",
			"#FF99E6", "#FF1A66",
			"#991AFF", "#FF33FF",
			"#4DB3FF", "#B34D4D",
			"#66664D", "#4DB380",
			"#E6B3B3", "#E64D66",
		]);
		$times = 1;
		// if ($id = $request->id){
		$id = $id_x;
		$kpi = $this->model->find($id);
		$label = ["Giao hàng", "Chất lượng", "Phản hồi", "Thanh toán", "Bảo hành"];
		$object1 = [
			"label" => "T-1",
			'data' => [5, 3, 4, 2, 3],
			"backgroundColor" => "rgba(91,155,213,.2)",
		];
		$object2 = [
			"label" => "T-2",
			'data' => [3, 4, 1, 1, 3],
			"backgroundColor" => "rgba(237,125,49,.2)",
		];
		$struct = [
			"id" => 'radar-' . $kpi->code,
			"name" => $kpi->name,
			'average_target' => round($kpi->getSumTarget() / $times, 2),
			'chart' => [
				"type" => "radar",
				"label" => $label,
				"object_1" => $object1,
				"object_2" => $object2,
			],
		];
		return $struct;
	}

	private function getLabel($percent = 0) {
		if ($percent < 50) {
			return 'progress-bar-success';
		} else if ($percent >= 50 && $percent < 75) {
			return 'progress-bar-warning';
		} else if ($percent >= 75) {
			return 'progress-bar-danger';
		}
	}

	private function divide($a = 0, $b = 0) {
		$result = 0;
		if ($b == 0) {
			return 100;
		} else {
			$result = round(($a * 100) / $b);
			return $result;
		}
	}

	public function home() {
		return redirect(auth()->user()->home_url);
	}
}
