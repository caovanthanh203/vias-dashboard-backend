<?php

namespace App\Http\Controllers;

use App\Models\QRow\QRow;
use Illuminate\Http\Request;

class QRowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QRow\QRow  $qRow
     * @return \Illuminate\Http\Response
     */
    public function show(QRow $qRow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QRow\QRow  $qRow
     * @return \Illuminate\Http\Response
     */
    public function edit(QRow $qRow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QRow\QRow  $qRow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QRow $qRow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QRow\QRow  $qRow
     * @return \Illuminate\Http\Response
     */
    public function destroy(QRow $qRow)
    {
        //
    }
}
