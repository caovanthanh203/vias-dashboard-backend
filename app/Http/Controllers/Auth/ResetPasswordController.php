<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\User\UserRepository;


class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

     private $userRepo;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
        $this->middleware('guest');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());
        
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        if ($user = $this->userRepo->findByField("email", $request->email)->first()) {
            
                $user->update([
                    // 'otp'       => null,
                    // 'is_active' => true,
                    'password'  => bcrypt($request->password),

                ]);
                $user->setRememberToken(Str::random(60));
                $user->save();
                $request->session()->flash('success', 'Đổi mật khẩu thành công, vui lòng đăng nhập!');
                return redirect('/login');
            
        }

        return $this->handleError(403, "Sai email");
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            //"otp"      => "required|string|digits:4",
            "email"    => "required|exists:users,email",
            'password' => 'required|string|min:6',
            'confirm_password' =>'required|same:password'
            
        ];
    }



    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
}
