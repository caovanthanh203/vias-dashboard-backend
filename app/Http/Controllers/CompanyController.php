<?php

namespace App\Http\Controllers;

use App\Models\Company\CompanyRepository;
use App\Models\User\UserRepository;
use App\Models\Company\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    private $model, $userRepo;
    
    public function __construct(CompanyRepository $model, UserRepository $userRepo)
    {
        $this->authCheck();
        $this->model = $model;   
        $this->userRepo = $userRepo;    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->model
        ->when($request->filled("search_key"), function ($q) use ($request) {
            return $q->where(function($user) use ($request){
                return $user->where("name", "like", "%" . $request->search_key . "%")
                ->orWhere("short_name", "like", "%".$request->search_key."%");
            });
        })
        ->setTransform(function ($item){
            return [
            'id'            => $item->id,
            'name'          => $item->name,
            'short_name'    => $item->short_name,
            'location_id'   => $item->location_id,
            'location_name' => $item->location_name,
            'admin'         => $item->getAdmin()
            ];
        })->paginate(15);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                        "company_name"          => "required|unique:companies,name",
                        "company_short_name"    => "required|alpha_dash|unique:companies,short_name",
                        "admin_id"              => "required|alpha_dash|unique:users,code",
                        "admin_name"            => "required",
                        "company_location"      => "required"
                        ]);
        $model_data = [
        'name'          => $request->company_name,
        'short_name'    => $request->company_short_name,
        'location_id'   => $request->company_location
        ];
        
        if ($model = $this->model->create($model_data)) {
            $otp = $this->genOtp(false);
            $password = bcrypt($otp);
            $admin_data = [
            'code'          => $request->admin_id,
            'name'          => $request->admin_name,
            'otp'           => $otp,
            'password'      => $password,
            'department_id' => 0,
            'company_id'    => $model['id']
            ];
            $this->userRepo->create($admin_data)->roles()->attach([1]);
            return $this->handleSuccess(trans('messages.company.success.created', ['name' => $model['name']]));
        }
    }

    public function update(Request $request)
    {
        $except = $request->id;
        $rule = [
        "company_name"          => "required|unique:companies,name,{$except}",
        "company_short_name"    => "required|alpha_dash|unique:companies,short_name,{$except}",
        "admin_id"              => "required|alpha_dash|unique:users,code",
        "admin_name"            => "required",
        "company_location"      => "required",
        "password"              => "required"
        ];
        if ($company = $this->model->find($request->id)){
            if ($company_admin = $company->hasAdmin()){
                $except_admin = $company_admin->id;
                $rule = [
                "company_name"          => "required|unique:companies,name,{$except}",
                "company_short_name"    => "required|alpha_dash|unique:companies,short_name,{$except}",
                "admin_id"              => "required|alpha_dash|unique:users,code,{$except_admin}",
                "admin_name"            => "required",
                "company_location"      => "required",
                "password"              => "required"
                ];
            }
        }
        $this->validate($request, $rule);
        if ($model = $this->model->find($request->id)) {
            $model->name         = $request->company_name;
            $model->short_name   = $request->company_short_name;
            $model->location_id  = $request->company_location;
            if ($admin = $model->hasAdmin()){
                $otp             = $request->password;
                $password        = bcrypt($otp);
                $admin->code     = $request->admin_id;
                $admin->name     = $request->admin_name;
                $admin->password = $password;
                $admin->otp      = $otp;
                $admin->save();
            } else {
                $otp             = $request->password;
                $password        = bcrypt($otp);
                $admin_data = [
                'code'          => $request->admin_id,
                'name'          => $request->admin_name,
                'otp'           => $otp,
                'password'      => $password,
                'department_id' => 0,
                'company_id'    => $model['id']
                ];
                $this->userRepo->create($admin_data)->roles()->attach([1]);
            }
            $model->save();
            return $this->handleSuccess(trans('messages.company.success.updated', ['name' => $model['name']]));
        } else {
            return $this->handleError(404, trans('messages.error.notfound'));
        }
    }

    public function delete(Request $request)
    {
        if ($item = $this->model->find($request->id)) {
            $item->delete();
            return $this->handleSuccess(trans('messages.company.success.deleted', ['name' => $item['name']]));
        }
        return $this->handleError(404, trans('messages.error.notfound'));
    }

    public function detail($id)
    {
        return $this->model->setTransform(function ($item) {
            return [
            'id'            => $item->id,
            'name'          => $item->name,
            'short_name'    => $item->short_name,
            'phone'         => $item->phone,
            'email'         => $item->email,
            'address'       => $item->address,
            'avatar'        => $item->avatar
            ];
        })->find($id);
    }

    public function employees(Request $request,$id)
    {
        $id_company = $id;
        return $this->userRepo->whereHas("roles", function ($q) {
            return $q->where('slug', 'admin');
        })->scopeQuery(function ($q) use ($id_company) {
            return $q->where("id_company", $id_company);
        })->when($request->filled("search_key"), function ($q) use ($request) {
            return $q->where(function($user) use ($request){
                return $user->where("name", "like", "%" . $request->search_key . "%");
            });
        })->orderBy('id','DESC')->setTransform(function ($item){
            return [
            'id'            => $item->id,
            'name'          => $item->name,
            'code' => $item->code,
            'otp' => $item->otp,

            ];
        })->paginate(15);
        
    }

    public function storeemployees(Request $request)
    {
        //dd($request->all());
        $id_company = $request->id_company;
        $this->validate($request, [
                        "code"          => "required|unique:users",
                        "name"    => "required",
                        ]);
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $password = substr(str_shuffle(str_repeat($pool, 6)), 0, 10);
        //dd($password);
        $model_data = [
        'name'          => $request->name,
        'code'    => $request->code,
        'department' => 0,
        'password' => bcrypt($password),
        'otp' => $password,
        'id_company' => $id_company,


        ];
        if ($model = $this->userRepo->create($model_data)) {
            $model->save();
            $model->roles()->attach([1]);
            return $this->handleSuccess('Thêm '.$model['name'].' thành công!');
        }
    }

    public function detailemployees($id)
    {   
        return $this->userRepo->setTransform(function ($item){
            return [
            'id'            => $item->id,
            'name'          => $item->name,
            'code' => $item->code,
            'department'    => 0,
            'role' => 1,
            'otp' => $item->otp,
            'password' => $item->otp,


            ];
        })->find($id);
    }

    public function updateemployees(Request $request)
    {
        //dd($request->all());

        $this->validate($request, [
                        "code"          => "required|unique:users,code," . $request->id,
                        "name"    => "required",
                        "password"    => "required",

                        ]);
        $model_data = $this->userRepo->find($request->id);
        //$model->roles()->detach([$model_data->department]);
        $model_data->name          = $request->name;
        $model_data->code    = $request->code;
        $model_data->department = 0;
        $model_data->password = bcrypt($request->password);
        $model_data->otp = $request->password;
        $model_data->save();
        $model_data->roles()->sync([1]);
        return $this->handleSuccess('Cập nhật '.$model_data['name'].' thành công!');
        
    }

}
