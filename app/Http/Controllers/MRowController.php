<?php

namespace App\Http\Controllers;

use App\Models\MRow\MRow;
use Illuminate\Http\Request;

class MRowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MRow\MRow  $mRow
     * @return \Illuminate\Http\Response
     */
    public function show(MRow $mRow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MRow\MRow  $mRow
     * @return \Illuminate\Http\Response
     */
    public function edit(MRow $mRow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MRow\MRow  $mRow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MRow $mRow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MRow\MRow  $mRow
     * @return \Illuminate\Http\Response
     */
    public function destroy(MRow $mRow)
    {
        //
    }
}
