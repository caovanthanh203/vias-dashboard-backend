<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Classes\Helpers;
use App\Classes\OtpType;
use Illuminate\Http\Request;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function handleError($code, $message, $errors = [])
	{
		return response(json_encode([
			'status'  => 0,
			'message' => $message,
			'errors'  => $errors
		]), $code)->header('Content-Type', 'application/json');
	}

	public function handleSuccess($message)
	{
		return [
			'status'  => 1,
			'message' => $message
		];
	}

	public function genOtp($toJson = true){
        $otp = Helpers::genRandomOtp(10, OtpType::Lower);
        if ($toJson){
        	return ["data" => $otp];
        } else {
        	return $otp;
        }
    }

    public function authCheck(){
    	$this->middleware(function ($request, $next) {
    		if (Auth::check()){
    			if (Auth::user()->hasAnyRole()){
    				return $next($request);
    			} else {
    				Auth::logout();
    			}
            }
            return $this->handleError(500, trans('messages.error.session'));
        });
    }
}
