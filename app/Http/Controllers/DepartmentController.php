<?php

namespace App\Http\Controllers;

use App\Models\Department\DepartmentRepository;
use App\Models\User\UserRepository;
use App\Models\Department\Department;
use Illuminate\Http\Request;
use Auth;


class DepartmentController extends Controller
{

    private $model, $userRepo;
    
    public function __construct(DepartmentRepository $model, UserRepository $userRepo)
    {
        $this->authCheck();
        $this->model = $model;   
        $this->userRepo = $userRepo;    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_company = auth()->user()->id_company;
        return $this->model->scopeQuery(function ($q) use ($id_company) {
            return $q->where("id_company", $id_company);
        })
        ->when($request->filled("search_key"), function ($q) use ($request) {
            return $q->where(function($user) use ($request){
                return $user->where("name", "like", "%" . $request->search_key . "%")->orWhere("short_name", "like", "%".$request->search_key."%");
            });
        })->orderBy('id','DESC')
        ->setTransform(function ($item){
            return [
            'id'            => $item->id,
            'name'          => $item->name,
            'amount_worker' => $item->amount_worker,
            'amount_kpi'    => $item->amount_kpi,
            
            ];
        })->paginate(15);
    }

    public function suggestions()
    {
        $user = auth()->user();
        $company_id = $user->company_id;
        return $this->model
        ->scopeQuery(function ($q) use ($company_id) {
            return $q->where("company_id",'=', $company_id);
        })
        ->orderBy('name','DESC')->setTransform(function ($item){
            return $item->name;
        })->get();
    }

    public function option()
    {
        $user = auth()->user();
        $company_id = $user->company_id;
        if($user->is_manager||$user->is_admin){
            return $this->model
                ->scopeQuery(function ($q) use ($company_id) {
                    return $q->where("company_id",'=', $company_id);
                })
                ->orderBy('name')
                ->setTransform(function($item){
                    return [
                        'id'    => $item->id,
                        'name'  => $item->name
                    ];
                })->get();
        }
    }


    public function select() {
        $options = [];
        $paging = false;
        $id_company = auth()->user()->id_company;
        $lists = $this->model
        ->scopeQuery(function ($q) use ($id_company) {
            return $q->where("id_company", $id_company);
        })
        ->when(request()->filled('search_key'), function ($query) {
            return $query->where(function ($query) {
                return $query->where('name', 'LIKE', '%' . request('search_key') . '%');
            });
        })
        ->setTransform(function ($item) {
            return [
            'id'             => 'department:'.$item->id,
            'text'           => $item->name
            ];
        })->get();

        $options[] = [
        "text"      => "Phòng Ban",
        "children"  => $lists
        ];

        $users = $this->userRepo
        ->when(request()->filled('search_key'), function ($query) {
            return $query->where(function ($query) {
                return $query->where('name', 'LIKE', '%' . request('search_key') . '%');
            });
        })
        ->setTransform(function ($item) {
            return [
            'id'             => 'user:'.$item->id,
            'text'           => $item->name
            ];
        })->get();

        $options[] = [
        "text"      => "Người Dùng", 
        "children"  => $users
        ];

        return [
        "results"    => $options,
        "pagination" => [
        "more" => $paging
        ]
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_company = auth()->user()->id_company;
        $this->validate($request, [
            "name"          => "required|unique:departments",
            "short_name"    => "required|unique:departments",
            ]);
        $model_data = [
        'name'          => $request->name,
        'short_name'    => $request->short_name,
        'id_company'    => $id_company,
        ];
        if ($model = $this->model->create($model_data)) {
            $model->save();
            return $this->handleSuccess('Thêm '.$model_data['name'].' thành công!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Request $request)
    // {
    //     if ($item = $this->model->find($request->id)) {
    //         $item->delete();
    //         return $this->handleSuccess(
    //             [
    //             'title' => 'Thành công',
    //             'text'  => "Phòng "
    //             . $item['name']
    //             . " đã được xóa"
    //             ]);
    //     }
    //     return $this->handleError(404, "Không tìm thấy phòng!");
    // }
}
