<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kpi\KpiRepository as KRepo;
use App\Models\ChartType\ChartTypeRepository as CRepo;
use App\Models\Target\TargetRepository as TRepo;
use App\Models\Department\DepartmentRepository as DRepo;
use App\Models\User\UserRepository as URepo;
use App\Models\QTarget\QTargetRepository as QTRepo;
use App\Models\MTarget\MTargetRepository as MTRepo;
use App\Models\QRow\QRowRepository as QRRepo;
use App\Models\MRow\MRowRepository as MRRepo;

class KpiController extends Controller
{

    private $model, $chartRepo, $dRepo, $uRepo, $tRepo, $mtRepo, $qtRepo, $mrRepo, $qrRepo;
    
    public function __construct(
        KRepo $model, CRepo $chartRepo, DRepo $dRepo, 
        URepo $uRepo, TRepo $tRepo, MTRepo $mtRepo, QTRepo $qtRepo, MRRepo $mrRepo, QRRepo $qrRepo) {
        $this->authCheck();
        $this->model = $model;   
        $this->chartRepo = $chartRepo;     
        $this->dRepo = $dRepo;
        $this->uRepo = $uRepo;
        $this->tRepo = $tRepo;
        $this->mtRepo = $mtRepo;
        $this->qtRepo = $qtRepo;
        $this->mrepo = $mrRepo;
        $this->qrRepo = $qrRepo;
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($kpi) {
            $kpi->qtarget->delete();
            $kpi->mtarget->delete();
            foreach ($kpi->targets()->mrows as $item) {
                $item->delete();
            }
            foreach ($kpi->targets()->qrows as $item) {
                $item->delete();
            }
            foreach ($kpi->targets as $item) {
                $item->delete();
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($user = auth()->user())
        {
            if ($user->is_manager)
            {
                return $this->model
                ->scopeQuery(function ($q) use ($user) {
                    return $q->where("company_id", $user->company_id);
                })
                ->when($request->filled("search_department"), function ($q) use ($request) {
                    return $q->where(function($user) use ($request){
                        return $user->where("department_id", $request->search_department);
                    });
                })
                ->when($request->filled("search_key"), function ($q) use ($request) {
                    return $q->where(function($user) use ($request){
                        return $user->where("name", "like", "%" . $request->search_key . "%")->orWhere("code", "like", "%".$request->search_key."%");
                    });
                })
                ->orderBy('name')
                ->setTransform(function ($item){
                    return [
                    "id"            => $item->id,
                    "code"          => strtoupper($item->code),
                    "name"          => $item->name,
                    "description"   => $item->description,
                    "res_depart"    => $item->target_department,
                    "user"          => $item->target_user,
                    "chart"         => $item->chart,
                    "init"          => $item->init
                    ];
                })->paginate(15);
            } else if ($user->is_generalinchief){
                return $this->model
                ->scopeQuery(function ($q) use ($user) {
                    return $q->where("company_id", $user->company_id)->where('department_id', $user->department_id);
                })
                ->when($request->filled("search_key"), function ($q) use ($request) {
                    return $q->where(function($user) use ($request){
                        return $user->where("name", "like", "%" . $request->search_key . "%")->orWhere("code", "like", "%".$request->search_key."%");
                    });
                })->orderBy('id','DESC')
                ->setTransform(function ($item){
                    return [
                    "id"            => $item->id,
                    "code"          => strtoupper($item->code),
                    "name"          => $item->name,
                    "description"   => $item->description,
                    "res_depart"    => $item->target_department,
                    "user"          => $item->target_user,
                    "chart"         => $item->chart,
                    "init"          => $item->init
                    ];
                })->paginate(15);
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    public function select() {
        $lists = $this->model
        ->when(request()->filled('search_key'), function ($query) {
            return $query->where(function ($query) {
                return $query->where('name', 'LIKE', '%' . request('search_key') . '%');
            });
        })
        ->setTransform(function ($item) {
            return [
            'id'             => $item->id,
            'text'           => $item->name
            ];
        })->paginate();
        return [
        "results"    => $lists->items(),
        "pagination" => [
        "more" => $lists->currentPage() < $lists->lastPage()
        ]
        ];
    }

    public function list() {
        if ($user = auth()->user()){
            $company_id = $user->company_id;
            $lists = $this->model
            ->scopeQuery(function ($q) use ($company_id) {
                return $q->where("company_id",'=', $company_id)->where("init", 1)->whereHas('targets');
            })
            ->setTransform(function ($item) {
                return [
                'id'             => $item->id,
                'text'           => $item->name_reduce
                ];
            })->get();
            return $lists;
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editTarget(Request $request)
    {
        if ($id = $request->id){
            $kpi = $this->model->find($id);
            $target = $kpi->getDataTarget();
            $label = $kpi->getDataLabel();
            return ['target' => $target, 'label' => $label];
        }
    }

    public function updateTarget(Request $request)
    {
        if ($id = $request->id){
            $kpi = $this->model->find($id);
            if ($kpi->time_frame == 4){
                $data_target = [
                'q1'    => $request->target[0],
                'q2'    => $request->target[1],
                'q3'    => $request->target[2],
                'q4'    => $request->target[3]
                ];
                $kpi->qtarget->update($data_target);
            } else {
                $data_target = [
                'm1'    => $request->target[0],
                'm2'    => $request->target[1],
                'm3'    => $request->target[2],
                'm4'    => $request->target[3],
                'm5'    => $request->target[4],
                'm6'    => $request->target[5],
                'm7'    => $request->target[6],
                'm8'    => $request->target[7],
                'm9'    => $request->target[8],
                'm10'   => $request->target[9],
                'm11'   => $request->target[10],
                'm12'   => $request->target[11]
                ];
                $kpi->mtarget->update($data_target);
            }
            return $this->handleSuccess('Cập nhật '.$kpi['name'].' thành công!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($user = auth()->user())
        {
            if ($user->is_generalinchief)
            {
                $company_id     = $user->company_id;
                $department_id  = $user->department_id;
                $creator_id     = $user->id;
                $this->validate($request, [
                                "kpi_code"          => "required|alpha_dash|unique:kpi,code",
                                "kpi_name"          => "required",
                                "kpi_owner"         => "required"
                                ]);
                $model_data = [
                'name'          => $request->kpi_name,
                'code'          => $request->kpi_code,
                'company_id'    => $company_id,
                'department_id' => $department_id,
                'creator_id'    => $creator_id
                ];

                if ($kpi = $this->model->create($model_data)){
                    if ($person = $this->uRepo->find($request->kpi_owner)){
                        $kpi->user()->associate($person);
                        $kpi->save();
                    }
                    return $this->handleSuccess(trans('messages.kpi.success.created', ['name' => $kpi['name']]));
                }
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    public function changeStatus(Request $request)
    {
        if ($user = auth()->user())
        {
            if ($user->is_generalinchief)
            {
                $kpi = $this->model->scopeQuery(function ($q) use ($user) {
                    return $q->where("company_id", $user->company_id)->where('department_id', $user->department_id);
                })->find($request->id);
                if ($kpi){
                    $kpi->is_open = !$kpi->is_open;
                    $kpi->save();
                    return $this->handleSuccess(trans('messages.kpi.success.updated', ['name' => $kpi['name']]));
                } 
                else {
                    return $this->handleError(404, trans('messages.error.notfound'));
                }
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }


    public function update(Request $request){
        if ($user = auth()->user())
        {
            if ($user->is_generalinchief)
            {
                $except = $request->id;
                $this->validate($request, [
                                "kpi_code"          => "required|alpha_dash|unique:kpi,code,{$except}",
                                "kpi_name"          => "required",
                                "kpi_owner"         => "required"
                                ]);
                if ($kpi = $this->model->find($request->id)){
                    $kpi->name = $request->kpi_name;
                    $kpi->code = $request->kpi_code;
                    $kpi->save();
                    if ($person = $this->uRepo->find($request->kpi_owner)){
                        $kpi->user()->associate($person);
                        $kpi->save();
                    }
                    return $this->handleSuccess(trans('messages.kpi.success.updated', ['name' => $kpi['name']]));
                }
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    public function edit(Request $request){
        if ($user = auth()->user())
        {
            if ($user->is_generalinchief || $user->is_staff)
            {
                if ($kpi = $this->model->find($request->id)){
                    if ($kpi->init){
                        return [
                            'id'            => $kpi->id,
                            'name'          => $kpi->name,
                            'chart_type_id' => $kpi->chart_type_id,
                            'description'   => $kpi->description,
                            'unit'          => $kpi->unit,
                            'time_frame'    => $kpi->time_frame,
                            'targets'       => $kpi->getObjectTarget(),
                            'time_targets'  => $kpi->getDataTargetPadded()            
                        ];
                    }
                    return $this->handleError(401, "unAuthorized");
                }
                return $this->handleError(404, trans('messages.error.notfound'));
            }
        }
        return $this->handleError(401, "unAuthorized");
    }

    public function init(Request $request, $create = true){
        if ($user = auth()->user())
        {
            if ($user->is_generalinchief || $user->is_staff)
            {
                $except = $request->id;
                $target_array = $request->kpi_target;
                if (is_array($target_array)){
                    $collect = collect($target_array)->filter();
                    $target_array = $collect->count()>0?$collect->toArray():null;
                };
                $request->merge(['kpi_target' => $target_array]);
                $this->validate($request, [
                                "kpi_description"   => "required",
                                "kpi_unit"          => "required",
                                "kpi_chart_type_id" => "required",
                                "kpi_time_frame"    => "required",
                                "kpi_target"        => "required" 
                                ]);
                if ($kpi = $this->model->find($request->id)){
                    //update attributes kpi
                    if ($kpi->init){
                        if ($create){
                            return $this->handleError(500, trans('messages.error.initting'));
                        }
                    } else {
                        if (!$create){
                            return $this->handleError(500, trans('messages.error.updating'));
                        }
                    }

                    $kpi->unit          = $request->kpi_unit;
                    $kpi->description   = $request->kpi_description;
                    $kpi->time_frame    = $request->kpi_time_frame;
                    $kpi->init          = true;

                    $target_names   = $request->kpi_target;
                    $kpi_id         = $kpi->id;
                    $target_ids     = [];
                    $target_models  = [];
                    $target_type    = null;
                    $target_rows    = [];

                    foreach ($target_names as $target_name) {
                        if ($target_name){
                            if ($t_model = $this->tRepo->create(['name' => $target_name])){
                                $target_ids[] = $t_model->id;
                                $target_models[] = $t_model;
                            }
                        }
                    }

                    if ($kpi->time_frame == 4){
                        $data_target = [
                        'q1'    => $request->kpi_time_frame_target[0],
                        'q2'    => $request->kpi_time_frame_target[1],
                        'q3'    => $request->kpi_time_frame_target[2],
                        'q4'    => $request->kpi_time_frame_target[3]
                        ];
                        $target_type = $this->qtRepo->create($data_target);

                        foreach ($target_models as $target) {
                            $target_rows[] = $target->qrows()->create(['kpi_id' => $kpi_id]);
                        }
                    } else {
                        $data_target = [
                        'm1'    => $request->kpi_time_frame_target[0],
                        'm2'    => $request->kpi_time_frame_target[1],
                        'm3'    => $request->kpi_time_frame_target[2],
                        'm4'    => $request->kpi_time_frame_target[3],
                        'm5'    => $request->kpi_time_frame_target[4],
                        'm6'    => $request->kpi_time_frame_target[5],
                        'm7'    => $request->kpi_time_frame_target[6],
                        'm8'    => $request->kpi_time_frame_target[7],
                        'm9'    => $request->kpi_time_frame_target[8],
                        'm10'   => $request->kpi_time_frame_target[9],
                        'm11'   => $request->kpi_time_frame_target[10],
                        'm12'   => $request->kpi_time_frame_target[11]
                        ];
                        $target_type = $this->mtRepo->create($data_target);

                        foreach ($target_models as $target) {
                            $target_rows[] = $target->mrows()->create(['kpi_id' => $kpi_id]);
                        }
                    }

                    $chart_type = $this->chartRepo->find($request->kpi_chart_type_id);

                    $process = false;
                    $mock_rows = true;
                    $process = ($target_type != null) 
                                && ($chart_type != null) 
                                && (count($target_rows) > 0) 
                                && (count($target_ids) > 0)
                                && (count($target_models) > 0);

                    if ($process){
                        if ($create){
                            $kpi->charttype()->associate($chart_type);
                            $kpi->targets()->attach($target_ids);
                            $target_type->kpi()->associate($kpi);
                            if ($kpi->save() && $target_type->save()){
                                return $this->handleSuccess(trans('messages.kpi.success.initted', ['name' => $kpi['name']])); 
                            } else {
                                return $this->handleError(500, trans('messages.error.initting'));
                            }
                        } else {
                            //clear all old relations
                            $kpi->charttype()->dissociate();   
                            $kpi->targets()->detach();
                            $status = true;
                            if ($qtargets = $kpi->qtargets) {
                                foreach ($qtargets as $target) {
                                    $status = $target->kpi()->dissociate()->save();
                                }
                            }
                            if ($mtargets = $kpi->mtargets) {
                                foreach ($mtargets as $target) {
                                    $status = $target->kpi()->dissociate()->save();
                                }
                            }
                            //add new relations
                            $kpi->charttype()->associate($chart_type);
                            $kpi->targets()->attach($target_ids);
                            $target_type->kpi()->associate($kpi);
                            if ($kpi->save() && $target_type->save() && $status){
                                return $this->handleSuccess(trans('messages.kpi.success.updated', ['name' => $kpi['name']])); 
                            } else {
                                return $this->handleError(500, trans('messages.error.updating'));
                            }
                        }
                    }
                    return $this->handleError(500, trans('messages.error.initting'));                
                }
                return $this->handleError(404, trans('messages.error.notfound'));
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    public function reInit(Request $request){
        return $this->init($request, false);
    }

    public function storebk(Request $request)
    {
        if ($user = auth()->user())
        {
            if ($user->is_generalinchief)
            {
                $company_id = auth()->user()->company_id;
                $department_id = auth()->user()->department;
                $creator_id = auth()->user()->id;
                $this->validate($request, [
                                "kpi_code"          => "required|alpha_dash|unique:kpi,code",
                                "kpi_name"          => "required",
                                "kpi_owner"         => "required"
                                ]);
                $model_data = [
                'name'          => $request->name,
                'code'          => Rand(),
                'unit'          => $request->unit,
                'description'   => $request->description,
                'time_frame'    => $request->time_frame,
                'company_id'    => $company_id,
                'department_id' => $department_id,
                'user_id'       => $user_id
                ];

                $target_names = $request->target;

                $target_ids = [];

                foreach ($target_names as $target_name) {
                    if (is_array($target_name) && $target_name["name"] != ""){
                        if ($t_model = $this->tRepo->create($target_name)){
                            $target_ids[] = $t_model->id;
                        }
                    }
                }

                if ($model = $this->model->create($model_data)) {
                    if ($chart_type = $this->chartRepo->find($request->chart_type_id)){
                        $model->charttype()->associate($chart_type);
                    }

                    $model->targets()->attach($target_ids);
                    $model->save();

                    $targets = $model->targets;
                    $kpi_id = $model->id;
                    if ($model->time_frame == 4){
                        $data_target = [
                        'q1'    => $request->kpi_time_frame_target[0],
                        'q2'    => $request->kpi_time_frame_target[1],
                        'q3'    => $request->kpi_time_frame_target[2],
                        'q4'    => $request->kpi_time_frame_target[3]
                        ];
                        $qtarget = $model->qtarget()->create($data_target);

                        foreach ($targets as $target) {
                            $qrow = $target->qrows()->create(['kpi_id' => $kpi_id]);
                        }
                    } else {
                        $data_target = [
                        'm1'    => $request->kpi_time_frame_target[0],
                        'm2'    => $request->kpi_time_frame_target[1],
                        'm3'    => $request->kpi_time_frame_target[2],
                        'm4'    => $request->kpi_time_frame_target[3],
                        'm5'    => $request->kpi_time_frame_target[4],
                        'm6'    => $request->kpi_time_frame_target[5],
                        'm7'    => $request->kpi_time_frame_target[6],
                        'm8'    => $request->kpi_time_frame_target[7],
                        'm9'    => $request->kpi_time_frame_target[8],
                        'm10'   => $request->kpi_time_frame_target[9],
                        'm11'   => $request->kpi_time_frame_target[10],
                        'm12'   => $request->kpi_time_frame_target[11]
                        ];
                        $mtarget = $model->mtarget()->create($data_target);

                        foreach ($targets as $target) {
                            $mrow = $target->mrows()->create(['kpi_id' => $kpi_id]);
                        }
                    }
                    return $this->handleSuccess('Thêm '.$model_data['name'].' thành công!');
                }
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($user = auth()->user())
        {
            if ($user->is_generalinchief || $user->is_staff)
            {
                $kpi = $this->model->scopeQuery(function ($q) use ($user) {
                    return $q->where("company_id", $user->company_id)->where('department_id', $user->department_id);
                })->find($id);
                if ($kpi){
                    if ($kpi->init){
                    } else {
                        //if not init yet
                        return ['id' => $kpi->id, 'name' => $kpi->name];
                    }
                } 
            }
        }
        return $this->handleError(401, "unAuthorized");
    }

    public function reportList(){
        if ($user = auth()->user()){
            $lists = [];
            if ($user->is_manager){
                $lists = $this->model
                ->scopeQuery(function ($item) use ($user){
                    return $item->where('company_id', $user->company_id);
                })->setTransform(function ($kpi) {
                    return [
                    'id'             => $kpi->id,
                    'current_time'   => $kpi->index_in_year,
                    'status'         => false,
                    'init'           => $kpi->init,
                    'name'           => $kpi->name,
                    'chart_type'     => $kpi->charttype?$kpi->charttype->icon:"",
                    'unit'           => $kpi->unit,
                    'struct'         => $kpi->formatted_struct
                    ];
                })->get();
                return $lists;
            } else if ($user->is_generalinchief){
                $lists = $this->model
                ->scopeQuery(function ($item) use ($user){
                    return $item->where('company_id', $user->company_id)
                    ->where('department_id', $user->department_id);
                })->setTransform(function ($kpi) {
                    return [
                    'id'             => $kpi->id,
                    'current_time'   => $kpi->index_in_year,
                    'status'         => false,
                    'unlocked'       => $kpi->is_unlocked,
                    'init'           => $kpi->init,
                    'name'           => $kpi->name,
                    'chart_type'     => $kpi->charttype?$kpi->charttype->icon:"",
                    'unit'           => $kpi->unit,
                    'struct'         => $kpi->formatted_struct
                    ];
                })->get();
                return $lists;
            } else if ($user->is_staff){
                $lists = $this->model
                ->scopeQuery(function ($item) use ($user){
                    return $item->where('company_id', $user->company_id)
                    ->where('department_id', $user->department_id)
                    ->where('user_id', $user->id);
                })->setTransform(function ($kpi) {
                    return [
                    'id'             => $kpi->id,
                    'current_time'   => $kpi->index_in_year,
                    'status'         => false,
                    'unlocked'       => $kpi->is_unlocked,
                    'init'           => $kpi->init,
                    'name'           => $kpi->name,
                    'chart_type'     => $kpi->charttype?$kpi->charttype->icon:"",
                    'unit'           => $kpi->unit,
                    'struct'         => $kpi->formatted_struct
                    ];
                })->get();
                return $lists;
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(Request $request)
    // {
    //     if ($id = $request->id){
    //         $kpi = $this->model->find($id);
    //         $target = $kpi->getDataTarget();
    //         $label = $kpi->getDataLabel();
    //         $data_target = $kpi->targets->transform(function ($item) use ($kpi){
    //             return [
    //             'id'     => $item->id,
    //             'object' => $item->name,
    //             'data'   => $item->getData($kpi->id, $kpi->time_frame)
    //             ];
    //         });

    //         return [
    //         'id' => $id, 
    //         'rows_target'   => $data_target,
    //         'target'        => $target, 
    //         'label'         => $label
    //         ];
    //     }
    // }

    private function struct($id)
    {
        if ($kpi = $this->model->find($id)){
            if ($kpi->init){
                $target = $kpi->getDataTarget();
                $label = $kpi->getDataLabel();
                $data_target = $kpi->targets->transform(function ($item) use ($kpi){
                    return [
                    'id'     => $item->id,
                    'object' => $item->name,
                    'data'   => $item->getData($kpi->id, $kpi->time_frame)
                    ];
                });
                return [
                'rows_target'   => $data_target,
                'target'        => $target, 
                'label'         => $label
                ];
            }
        }
        return [];
    }

    public function save(Request $request){
        if ($kpi_id = $request->id){
            $kpi = $this->model->find($kpi_id);
            $rows_target = $request->rows_target;
            $rows_target = is_array($rows_target)?$rows_target:[];
            if ($kpi->time_frame == 4){
                $pads = [null, null, null, null];
                //targets
                foreach ($rows_target as $row) {
                    $target_id = $row['id'];
                    $data = collect($row['data']);
                    $data = $data
                    ->union($pads)
                    ->keyBy(function ($item, $key) {
                        return ('q'.($key+1));
                    })->filter(function ($value, $key) {
                        return is_numeric($value);
                    })->toArray();
                    $qrow = $this->tRepo->find($target_id)
                    ->qrows()->where('kpi_id', $kpi_id)->first();
                    $qrow->update($data);
                }
            } else {
                $pads = [null, null, null, null, null, null, null, null, null, null, null, null];
                //targets
                foreach ($rows_target as $row) {
                    $target_id = $row['id'];
                    $data = collect($row['data']);
                    $data = $data
                    ->union($pads)
                    ->keyBy(function ($item, $key) {
                        return ('m'.($key+1));
                    })->filter(function ($value, $key) {
                        return is_numeric($value);
                    })->toArray();
                    $mrow = $this->tRepo->find($target_id)
                    ->mrows()->where('kpi_id', $kpi_id)->first();
                    $mrow->update($data);
                }
            }
            $kpi->is_open = false;
            $kpi->save();
            return $this->handleSuccess(trans('messages.kpi.success.updated', ['name' => $kpi['name']]));
        };
        return $this->handleError(500, trans('messages.error.updating'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if ($item = $this->model->find($request->id)) {
            $item->delete();
            return $this->handleSuccess(trans('messages.kpi.success.deleted', ['name' => $item['name']]));
        }
        return $this->handleError(404, trans('messages.error.notfound'));
    }
}
