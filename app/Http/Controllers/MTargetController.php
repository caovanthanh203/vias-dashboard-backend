<?php

namespace App\Http\Controllers;

use App\Models\MTarget\MTarget;
use Illuminate\Http\Request;

class MTargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MTarget\MTarget  $mTarget
     * @return \Illuminate\Http\Response
     */
    public function show(MTarget $mTarget)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MTarget\MTarget  $mTarget
     * @return \Illuminate\Http\Response
     */
    public function edit(MTarget $mTarget)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MTarget\MTarget  $mTarget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MTarget $mTarget)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MTarget\MTarget  $mTarget
     * @return \Illuminate\Http\Response
     */
    public function destroy(MTarget $mTarget)
    {
        //
    }
}
