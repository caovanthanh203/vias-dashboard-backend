<?php

namespace App\Http\Controllers;

use App\Models\User\UserRepository;
use App\Models\Department\DepartmentRepository;
use App\Models\Kpi\KpiRepository;
use App\Models\Kpi\Kpi;
use App\Models\User\User;
use Illuminate\Http\Request;
use Auth;

class EmployeesController extends Controller
{

    private $model;

    public function __construct(UserRepository $model,DepartmentRepository $depar,KpiRepository $kpi)
    {
        $this->authCheck();
        $this->model = $model;     
        $this->depar = $depar;     
        $this->kpi = $kpi;     
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->roles()->delete();
        });
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        $company_id = $user->company_id;
        $department_id = $user->department_id;
        if($user->is_admin){
            return $this->model
            ->whereHas("roles", function ($q) {
                return $q->where('slug', 'staff')->orwhere('slug', 'manager')->orwhere('slug', 'generalinchief');
            })
            ->scopeQuery(function ($q) use ($company_id) {
                return $q->where("company_id",'=', $company_id);
            })
            ->when($request->filled("search_role"), function ($user) use ($request) {
                return $user->whereHas("roles", function ($q) use ($request){
                    return $q->where('id', $request->search_role);
                });
            })
            ->when($request->filled("search_department"), function ($q) use ($request) {
                    return $q->where(function($user) use ($request){
                        return $user->where("department_id", $request->search_department);
                    });
            })
            ->when($request->filled("search_key"), function ($q) use ($request) {
                return $q->where(function($user) use ($request){
                    return $user->where("name", "like", "%" . $request->search_key . "%")
                    ->orWhere("code", "like", "%" . $request->search_key . "%");
                });
            })->orderBy('id','DESC')->setTransform(function ($item){
                return [
                'id'			    => $item->id,
                'name'              => $item->name,
                'code'              => $item->code,
                'department_name'   => $item->department->name,
                'department_id'     => $item->department->id,
                'role'              => $item->max_role,
                'password'          => $item->otp
                ];
            })->paginate(15);
        }
        else if($user->is_manager){
            return $this->model
            ->whereHas("roles", function ($q) {
                return $q->where('slug', 'staff')->orwhere('slug', 'generalinchief');
            })
            ->scopeQuery(function ($q) use ($company_id) {
                return $q->where("company_id",'=', $company_id);
            })
            ->when($request->filled("search_role"), function ($user) use ($request) {
                return $user->whereHas("roles", function ($q) use ($request){
                    return $q->where('id', $request->search_role);
                });
            })
            ->when($request->filled("search_department"), function ($q) use ($request) {
                return $q->where(function($user) use ($request){
                    return $user->where("department_id", $request->search_department);
                });
            })
            ->when($request->filled("search_key"), function ($q) use ($request) {
                return $q->where(function($user) use ($request){
                    return $user
                    ->where("name", "like", "%" . $request->search_key . "%")
                    ->orWhere("code", "like", "%" . $request->search_key . "%");
                });
            })->orderBy('id','DESC')->setTransform(function ($item){
                return [
                'id'                => $item->id,
                'name'              => $item->name,
                'code'              => $item->code,
                'department_name'   => $item->department->name,
                'department_id'     => $item->department->id,
                'role'              => $item->max_role
                ];
            })->paginate(15);
        }
        else if($user->is_generalinchief){
            return $this->model->whereHas("roles", function ($q) {
                return $q->where('slug', 'staff');
            })->scopeQuery(function ($q) use ($company_id) {
                return $q->where("company_id",'=', $company_id);
            })->scopeQuery(function ($q) use ($department_id) {
                return $q->where("department_id",'=', $department_id);
            })->when($request->filled("search_key"), function ($q) use ($request) {
                return $q->where(function($user) use ($request){
                    return $user->where("name", "like", "%" . $request->search_key . "%")
                    ->orWhere("code", "like", "%" . $request->search_key . "%");
                });
            })->orderBy('id','DESC')->setTransform(function ($item){
                return [
                'id'                => $item->id,
                'name'              => $item->name,
                'code'              => $item->code,
                'role'              => $item->max_role,
                'password'          => $item->otp
                ];
            })->paginate(15);
        }
    }

    public function option()
    {
        $user = auth()->user();
        $company_id = $user->company_id;
        $department_id = $user->department_id;
        if($user->is_generalinchief){
            return $this->model->whereHas("roles", function ($q) {
                return $q->where('slug', 'staff');
            })->scopeQuery(function ($q) use ($company_id) {
                return $q->where("company_id",'=', $company_id);
            })->scopeQuery(function ($q) use ($department_id) {
                return $q->where("department_id",'=', $department_id);
            })->orderBy('name','DESC')->setTransform(function ($item){
                return [
                'id'                => $item->id,
                'name'              => $item->name,
                ];
            })->get();
        }
    }

    public function department()
    {
        $id_company = auth()->user()->id_company;
        return $this->depar->scopeQuery(function ($q) use ($id_company) {
            return $q->where("company_id", $id_company);
        })
        ->setTransform(function ($item){
            return [
            'id'            => $item->id,
            'name'          => $item->name,
            ];
        })->get();
    }

    public function detail($id)
    {   
        $id_company = auth()->user()->id_company;
        return $this->model->scopeQuery(function ($q) use ($id_company) {
            return $q->where("id_company", $id_company);
        })->setTransform(function ($item){
            return [
            'id'            => $item->id,
            'name'          => $item->name,
            'code' => $item->code,
            'department'    => $item->departments->id,
            "kpi"           => $item->kpi()->pluck("id"),
            'kpi_defaults' => $item->kpi->transform(function($item){
                return[
                'id'            => $item->id
                ];
            }),
            'role' => $item->roles->first()->id,
            'otp' => $item->otp,
            'password' => $item->otp,


            ];
        })->find($id);
    }

    public function kpi()
    {
        $id_company = auth()->user()->id_company;
        $deparment = auth()->user()->deparment;
        if($deparment==0)
        {
            return $this->kpi->scopeQuery(function ($q) use ($id_company) {
                return $q->where("id_company", $id_company);
            })
            ->setTransform(function ($item){
                return [
                'id'            => $item->id,
                'name'          => $item->name,
                ];
            })->get();
        }
        else{
            return $this->kpi->scopeQuery(function ($q) use ($id_company) {
                return $q->where("id_company", $id_company);
            })->scopeQuery(function ($q) use ($id_company) {
                return $q->where("deparment", $deparment);
            })
            ->setTransform(function ($item){
                return [
                'id'            => $item->id,
                'name'          => $item->name,
                ];
            })->get();
        }
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        if ($user = auth()->user())
        {
            if ($user->is_admin)
            {
                $this->validate($request, [
                                "person_name"               => "required",
                                "person_code"               => "required|alpha_dash|unique:users,code",
                                "person_role"               => "required",
                                "person_department_name"    => "required"
                                ]);
                if ($department = $this->depar
                    ->scopeQuery(function ($q) use ($user) {return $q->where("company_id", $user->company_id);})
                    ->findByField('name', ['name' => $request->person_department_name])->first())
                {
                    $otp = $this->genOtp(false);
                    $password = bcrypt($otp);
                    $person_data = [
                    'name'          => $request->person_name,
                    'code'          => $request->person_code,
                    'password'      => $password,
                    'otp'           => $otp,
                    'department_id' => $department->id,
                    'company_id'    => $user->company_id
                    ];
                    if ($person = $this->model->create($person_data)){
                        $person->roles()->attach([$request->person_role]);
                        return $this->handleSuccess(trans('messages.organization.success.created', ['name' => $person['name']]));
                    }
                } else 
                {
                    $department_data = [
                    'name'          => $request->person_department_name,
                    'company_id'    => $user->company_id
                    ];
                    if ($department = $this->depar->create($department_data)){
                        $otp = $this->genOtp(false);
                        $password = bcrypt($otp);
                        $person_data = [
                        'name'          => $request->person_name,
                        'code'          => $request->person_code,
                        'password'      => $password,
                        'otp'           => $otp,
                        'department_id' => $department->id,
                        'company_id'    => $user->company_id
                        ];
                        if ($person = $this->model->create($person_data))
                        {
                            $person->roles()->attach([$request->person_role]);
                            return $this->handleSuccess(trans('messages.organization.success.created', ['name' => $person['name']]));
                        }
                    }
                }
                return $this->handleError(500, trans('messages.error.creating'));
            } else if ($user->is_generalinchief)
            {
                $this->validate($request, [
                                "person_name"               => "required",
                                "person_code"               => "required|alpha_dash|unique:users,code"                                ]);
                $otp = $this->genOtp(false);
                $password = bcrypt($otp);
                $person_data = [
                'name'          => $request->person_name,
                'code'          => $request->person_code,
                'password'      => $password,
                'otp'           => $otp,
                'department_id' => $user->department_id,
                'company_id'    => $user->company_id
                ];
                if ($person = $this->model->create($person_data)){
                    $person->roles()->attach([4]);
                    return $this->handleSuccess(trans('messages.organization.success.created', ['name' => $person['name']]));
                }
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    public function storev2(Request $request)
    {
        $id_company = auth()->user()->id_company;
        $this->validate($request, [
                        "code"          => "required|unique:users",
                        "name"    => "required",
                        "department"    => "required",
                        "role"    => "required",

                        ]);
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $password = substr(str_shuffle(str_repeat($pool, 6)), 0, 10);
        $model_data = [
        'name'          => $request->name,
        'code'    => $request->code,
        'department' => $request->department,
        'password' => bcrypt($password),
        'otp' => $password,
        'id_company' => $id_company,
        ];
        if ($model = $this->model->create($model_data)) {
            $model->save();
            $model->roles()->attach([$request->role]);
            if ($request->filled("kpi")) {
                $model->kpi()->attach($request->kpi);
            }
            return $this->handleSuccess('Thêm '.$model['name'].' thành công!');
        }
    }

    public function update(Request $request)
    {
        if ($user = auth()->user())
        {
            if ($user->is_admin)
            {
                $except = $request->id;
                $this->validate($request, [
                                "person_name"               => "required",
                                "person_code"               => "required|alpha_dash|unique:users,code,{$except}",
                                "person_role"               => "required",
                                "person_department_name"    => "required",
                                "password"                  => "required"
                                ]);
                if ($person = $this->model->find($request->id))
                {
                    if ($department = $this->depar
                        ->scopeQuery(function ($q) use ($user) {return $q->where("company_id", $user->company_id);})
                        ->findByField('name', ['name' => $request->person_department_name])->first())
                    {
                        $otp                    = $request->password;
                        $password               = bcrypt($otp);
                        $person->name           = $request->person_name;
                        $person->code           = $request->person_code;
                        $person->otp            = $otp;
                        $person->password       = $password;
                        $person->department_id  = $department->id;
                        if ($person->save()){
                            $person->roles()->sync($request->person_role);
                            return $this->handleSuccess(trans('messages.organization.success.updated', ['name' => $person['name']]));
                        }
                    } else 
                    {
                        $department_data = [
                        'name'          => $request->person_department_name,
                        'company_id'    => $user->company_id
                        ];
                        if ($department = $this->depar->create($department_data)){
                            $otp                    = $request->password;
                            $password               = bcrypt($otp);
                            $person->name           = $request->person_name;
                            $person->code           = $request->person_code;
                            $person->otp            = $otp;
                            $person->password       = $password;
                            $person->department_id  = $department->id;
                            if ($person->save()){
                                $person->roles()->sync($request->person_role);
                                return $this->handleSuccess(trans('messages.organization.success.updated', ['name' => $person['name']]));
                            }
                        }
                    }
                    return $this->handleError(500, trans('messages.error.updating'));
                } else {
                    return $this->handleError(404, trans('messages.error.notfound'));
                }
            } else if ($user->is_generalinchief)
            {
                $except = $request->id;
                $this->validate($request, [
                                "person_name"               => "required",
                                "person_code"               => "required|alpha_dash|unique:users,code,{$except}",
                                "password"                  => "required"
                                ]);
                if ($person = $this->model->find($request->id))
                {
                    $otp                    = $request->password;
                    $password               = bcrypt($otp);
                    $person->name           = $request->person_name;
                    $person->code           = $request->person_code;
                    $person->otp            = $otp;
                    $person->password       = $password;
                    if ($person->save()){
                        return $this->handleSuccess(trans('messages.organization.success.updated', ['name' => $person['name']]));
                    }
                    return $this->handleError(500, trans('messages.error.updating'));
                }
            }
        } else {
            return $this->handleError(401, "unAuthorized");
        }
    }

    public function show(Department $department)
    {
    }

    public function edit(Department $department)
    {
    }

    public function destroy(Request $request)
    {
        if ($item = $this->model->find($request->id)) {
            $item->delete();
            return $this->handleSuccess(trans('messages.organization.success.deleted', ['name' => $item['name']]));
        }
        return $this->handleError(404, trans('messages.error.notfound'));
    }
}
