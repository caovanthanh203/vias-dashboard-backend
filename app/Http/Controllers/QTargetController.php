<?php

namespace App\Http\Controllers;

use App\Models\QTarget\QTarget;
use Illuminate\Http\Request;

class QTargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QTarget\QTarget  $qTarget
     * @return \Illuminate\Http\Response
     */
    public function show(QTarget $qTarget)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QTarget\QTarget  $qTarget
     * @return \Illuminate\Http\Response
     */
    public function edit(QTarget $qTarget)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QTarget\QTarget  $qTarget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QTarget $qTarget)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QTarget\QTarget  $qTarget
     * @return \Illuminate\Http\Response
     */
    public function destroy(QTarget $qTarget)
    {
        //
    }
}
