<?php

namespace App\Http\Controllers;

use App\Models\ChartType\ChartTypeRepository;
use Illuminate\Http\Request;

class ChartTypeController extends Controller
{

    private $model;
    
    public function __construct(ChartTypeRepository $model)
    {
        $this->authCheck();
        $this->model = $model;        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->model
        ->setTransform(function ($item){
            return [
            "id"            => $item->id,
            "name"          => $item->name,
            "icon"          => $item->icon
            ];
        })->paginate(15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChartType\ChartType  $chartType
     * @return \Illuminate\Http\Response
     */
    public function show(ChartType $chartType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChartType\ChartType  $chartType
     * @return \Illuminate\Http\Response
     */
    public function edit(ChartType $chartType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChartType\ChartType  $chartType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChartType $chartType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChartType\ChartType  $chartType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChartType $chartType)
    {
        //
    }
}
