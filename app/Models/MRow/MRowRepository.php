<?php

namespace App\Models\MRow;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class MRowRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return MRow::class;
    }   
}
