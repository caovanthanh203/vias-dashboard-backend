<?php

namespace App\Models\User;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository implements RepositoryInterface
{
	
	
    public function model()
    {
        return User::class;
    }
    
}
