<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\Models\Role\Role;
use App\Models\Department\Department;
use App\Models\Company\Company;
use App\Models\Kpi\Kpi;
use App\Models\QRow\QRow;
use App\Models\MRow\MRow;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
	protected $fillable = [
	'name', 'code', 'department_id','otp','password','company_id', 'home_kpi'
	];

	protected $casts = [
	"home_kpi" => "array"
	];

	protected $hidden = [
	'password', 'remember_token',
	];

	public static function boot()
	{
		parent::boot();

		static::deleting(function ($user) {
			$user->roles()->detach();
		});
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'user_roles');
	}

	public function kpi()
	{
		return $this->belongsToMany(Kpi::class, 'creator_id');
	}

	public function poolkpis()
	{
		return $this->hasMany(Kpi::class);
	}

	public function department()
	{
		return $this->belongsTo(Department::class)->withDefault(['name' => 'N/A', 'id' => 'N/A']);
	}

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function getRoleSlugAttribute()
	{
		return $this->roles->first()?$this->roles->first():(new Role);
	}

	public function getIsAdminAttribute()
	{
		return $this->canRole('admin');
	}

	public function getIsManagerAttribute()
	{
		return $this->canRole('manager');
	}

	public function getIsGeneralinchiefAttribute()
	{
		return $this->canRole('generalinchief');
	}

	public function getIsStaffAttribute()
	{
		return $this->canRole('staff');
	}

	public function getHomeUrlAttribute(){
		if ($this->canRole('superadmin')){
			return '/#/company';
		} 
		elseif ($this->canRole('admin'))
		{
			return '/#/line-view';
		}
		elseif ($this->canRole('manager'))
		{
			return '/#/line-view';
		}
		elseif ($this->canRole('generalinchief'))
		{
			return '/#/line-view';
		}
		elseif ($this->canRole('staff'))
		{
			return '/#/line-view';
		} else {
			return '/logout';
		}
	}

	public function getShortNameAttribute(){
		return $this->shortString($this->name);
	}

	public function hasAnyRole()
	{
		if ($this->roles->count()>0){
			return true;
		};
		return false;
	}

	public function canRole($role = null)
	{
		// dd($this->roles->pluck('slug'));
		if (!is_null($role) && in_array($role, array_values($this->roles->pluck('slug')->toArray()))) {
			return true;
		}
		return false;
	}

	public function getMaxRoleAttribute(){
		if ($this->canRole('superadmin')){
			return ['id' => 5, 'name' => 'SuperAdmin'];
		} 
		elseif ($this->canRole('admin'))
		{
			return ['id' => 1, 'name' => 'Admin'];
		}
		elseif ($this->canRole('manager'))
		{
			return ['id' => 2, 'name' => 'Giám Đốc'];
		}
		elseif ($this->canRole('generalinchief'))
		{
			return ['id' => 3, 'name' => 'Trưởng Phòng'];
		}
		elseif ($this->canRole('staff'))
		{
			return ['id' => 4, 'name' => 'Nhân Viên'];
		} else {
			return ['id' => 'N/A', 'name' => 'N/A'];
		}
	}

	public function shortString($string = ""){
		$string = ucfirst($string);
		$words = explode(" ", $string);
		$last = array_pop($words);
		$result = "";
		foreach ($words as $w) {
			$result .= $w[0].".";
		};
		$result .= " ".$last;
		return $result;
	}
}
