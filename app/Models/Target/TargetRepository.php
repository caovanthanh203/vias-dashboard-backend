<?php

namespace App\Models\Target;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class TargetRepository extends BaseRepository implements RepositoryInterface
{
	
    public function model()
    {
        return Target::class;
    }
    
}
