<?php

namespace App\Models\Target;

use App\Models\QRow\QRow;
use App\Models\MRow\MRow;
use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $fillable = ['name'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($target) {
            $target->mrows()->delete();
            $target->qrows()->delete();
        });
    }

    public function qrows()
    {
        return $this->morphMany(QRow::class, 'qrowable');
    }

    public function mrows()
    {
        return $this->morphMany(MRow::class, 'mrowable');
    }

    public function getData($kpi_id, $time_frame)
    {
        if ($time_frame == 4){
            return $this->getQRow($kpi_id);
        } else {
            return $this->getMRow($kpi_id);
        };
    }

    private function getQRow($kpi_id){
        $qrow = $this->qrows()
        ->where('kpi_id', $kpi_id)
        ->get()
        ->transform(function ($item) {
            return [
            $item->q1,
            $item->q2,
            $item->q3,
            $item->q4
            ];
        })->flatten()->toArray();
        if (count($qrow) == 4){
            return $qrow;
        } else {
            return [-1, -1, -1, -1];
        };
    }

    private function getMRow($kpi_id){
        $mrow = $this->mrows()
        ->where('kpi_id', $kpi_id)
        ->get()
        ->transform(function ($item) {
            return [
            $item->m1,
            $item->m2,
            $item->m3,
            $item->m4,
            $item->m5,
            $item->m6,
            $item->m7,
            $item->m8,
            $item->m9,
            $item->m10,
            $item->m11,
            $item->m12
            ];
        })->flatten()->toArray();
        if (count($mrow) == 12){
            return $mrow;
        } else {
            return [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
        };
    }
}
