<?php

namespace App\Models\ChartType;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class ChartTypeRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return ChartType::class;
    }   
}
