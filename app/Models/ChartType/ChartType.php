<?php

namespace App\Models\ChartType;

use Illuminate\Database\Eloquent\Model;

class ChartType extends Model
{
    protected $fillable = ['name', 'icon'];
}
