<?php

namespace App\Models\Company;

use App\Models\User\User;
use App\Models\Kpi\Kpi;
use App\Models\Location\Location;
use App\Models\Images\Image;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	
	protected $fillable = [
	'name', 'short_name', 'location_id'
	];

	protected $table = "companies";

	public static function boot()
	{
		parent::boot();

		static::deleting(function ($company) {
			$company->users()->delete();
		});
	}

	public function imgAvatar()
	{
		return $this->morphOne(Image::class, 'imageable');
	}

	public function kpis(){
		return $this->hasMany(Kpi::class);
	}

	public function users(){
		return $this->hasMany(User::class);
	}

	public function location(){
		return $this->belongsTo(Location::class);
	}

	public function admins(){
		$admins = $this->users()->whereHas('roles', function ($roles) {
			return $roles->where('slug', 'admin');
		});
		return $admins;
	}

	public function getLocationNameAttribute(){
		return $this->location?$this->location->name:'';
	}

	public function getAdmin(){
		$admin = $this->admins()->first();
		if (!$admin) {
			$admin = ['name' => '', 'code' => ''];
			return $admin;
		};
		return $admin->only(['name', 'code', 'otp']);
	}

	public function hasAdmin(){
		$admin = $this->admins()->first();
		if (!$admin) {
			return null;
		};
		return $admin;
	}

	public function setAvatarAttribute($value)
	{
		if (isset($this->imgAvatar)) {
			$this->imgAvatar()->delete();
		}
		$this->imgAvatar()->create(["name" => $value]);
		
	}
	
	public function getAvatarAttribute()
	{
		if (isset($this->imgAvatar->name)) {
			return $this->imgAvatar->name;
		}
		
		return asset('/images/ic_noavt.png');
	}

	
}
