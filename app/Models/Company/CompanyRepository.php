<?php

namespace App\Models\Company;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class CompanyRepository extends BaseRepository implements RepositoryInterface
{
    public function model()
    {
        return Company::class;
    }
}
