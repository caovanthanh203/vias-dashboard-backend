<?php

namespace App\Models\Department;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class DepartmentRepository extends BaseRepository implements RepositoryInterface
{
    public function model()
    {
        return Department::class;
    }
}
