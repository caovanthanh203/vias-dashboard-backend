<?php

namespace App\Models\Department;

use Illuminate\Database\Eloquent\Model;
use App\Models\QRow\QRow;
use App\Models\MRow\MRow;
use App\Models\Kpi\Kpi;
use App\Models\User\User;

class Department extends Model
{
	protected $fillable = ['name', 'company_id'];

    public function users(){
        return $this->hasMany(User::class, 'department')->where('company_id', $this->id_company);
    }

    public function kpis(){
        return $this->hasMany(Kpi::class)->where('company_id', $this->id_company);
    }
	
    public function getAmountWorkerAttribute()
    {
    	return $this->users->count();
    }

    public function getAmountKpiAttribute()
    {
    	return 0;//$this->users()->kpi()->count();
    }
}
