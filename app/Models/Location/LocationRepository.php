<?php

namespace App\Models\Location;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class LocationRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return Location::class;
    }   
}
