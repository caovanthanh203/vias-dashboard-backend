<?php

namespace App\Models\MTarget;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class MTargetRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return MTarget::class;
    }   
}
