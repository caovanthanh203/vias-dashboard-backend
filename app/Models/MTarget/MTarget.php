<?php

namespace App\Models\MTarget;

use App\Models\Kpi\Kpi;
use Illuminate\Database\Eloquent\Model;

class MTarget extends Model
{
    protected $fillable = ['m1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10', 'm11', 'm12'];

    public function kpi()
	{
		return $this->belongsTo(Kpi::class);
	}
}
