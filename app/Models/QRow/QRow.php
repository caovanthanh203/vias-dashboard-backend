<?php

namespace App\Models\QRow;

use Illuminate\Database\Eloquent\Model;

class QRow extends Model
{
	protected $fillable = ['kpi_id', 'q1', 'q2', 'q3', 'q4'];
	
    public function qrowable()
    {
        return $this->morphTo();
    }
}
