<?php

namespace App\Models\QRow;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class QRowRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return QRow::class;
    }   
}
