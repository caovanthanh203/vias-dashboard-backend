<?php

namespace App\Models\Role;

use App\Models\User\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	use Sluggable;

    protected $fillable = [
        'name', 'slug'
    ];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_roles');
    }

    public function getSlugFormatAttribute()
    {
        return $this->name;
    }
    
}
