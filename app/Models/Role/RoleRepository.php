<?php

namespace App\Models\Role;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class RoleRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return Role::class;
    }   
}
