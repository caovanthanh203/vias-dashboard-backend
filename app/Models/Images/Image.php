<?php

namespace App\Models\Images;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Storage;

class Image extends Model
{
    protected $fillable = ["name"];

	public static function boot()
	{
		parent::boot();

		static::deleting(function ($image) {
			Storage::delete($image->attributes["name"]);
		});
	}
	
	public function getHasLocalImageAttribute() {
		return isset($this->attributes["name"]);
	}

	public function imageable()
	{
		return $this->morphTo();
	}

	public function setNameAttribute($value)
	{
		if($value instanceof UploadedFile) {
			if (isset($this->attributes["name"])) {
				Storage::delete($this->attributes["name"]);
			}
			$this->attributes["name"] = $value->storePublicly('public');
		}
		elseif (Str::startsWith($value, "http")) {
			$this->attributes["name"] = $value;
		}
	}

	public function getNameAttribute($value)
	{
		if(Str::startsWith($value, "http")) {
			return $value;
		}
		return $value ? asset(Storage::url($value)) : null;
	}
	
	public function getFileNameAttribute()
	{
		if(Str::startsWith($this->attributes["name"], "http")) {
			return $this->attributes["name"];
		}
		return $this->attributes["name"] ? Storage::url($this->attributes["name"]) : null;
	}
}
