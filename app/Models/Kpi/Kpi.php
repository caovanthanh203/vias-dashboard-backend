<?php

namespace App\Models\Kpi;

use App\Models\ChartType\ChartType;
use App\Models\Department\Department;
use App\Models\MTarget\MTarget;
use App\Models\QTarget\QTarget;
use App\Models\Target\Target;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Kpi extends Model {
	protected $fillable = [
		'name', 'code', 'description', 'chart_type_id', 'time_frame', 'company_id', 'unit', 'department_id', 'creator_id', 'is_open',
	];

	protected $table = "kpi";

	public static function boot() {
		parent::boot();
		static::deleting(function ($kpi) {
			$kpi->qtarget()->delete();
			$kpi->mtarget()->delete();
			$kpi->targets()->delete();
			$kpi->targets()->detach();
		});
	}

	public function targets() {
		return $this->belongsToMany(Target::class, 'target_kpi');
	}

	public function user() {
		return $this->belongsTo(User::class)->withDefault(['name' => '', 'id' => ""]);
	}

	public function creator() {
		return $this->belongsTo(User::class, 'creator_id')->withDefault(['name' => 'unknown']);
	}

	public function department() {
		return $this->belongsTo(Department::class, 'department_id')->withDefault(['name' => 'unknown']);
	}

	public function charttype() {
		return $this->belongsTo(ChartType::class, 'chart_type_id');
	}

	public function qtarget() {
		return $this->hasOne(QTarget::class)->withDefault(['q1' => 0, 'q2' => 0, 'q3' => 0, 'q4' => 0]);
	}

	public function mtarget() {
		return $this->hasOne(MTarget::class)->withDefault(['m1' => 0, 'm2' => 0, 'm3' => 0, 'm4' => 0, 'm5' => 0, 'm6' => 0, 'm7' => 0, 'm8' => 0, 'm9' => 0, 'm10' => 0, 'm11' => 0, 'm12' => 0]);
	}

	public function qtargets() {
		return $this->hasMany(QTarget::class);
	}

	public function mtargets() {
		return $this->hasMany(MTarget::class);
	}

	public function getTargetUserAttribute() {
		return $this->user;
	}

	public function getTargetDepartmentAttribute() {
		return $this->department->name;
	}

	public function getMonthRecordAttribute() {
		return "Month Records";
	}

	public function getQuarterYearRecordAttribute() {
		return "Quarter Year Records";
	}

	public function getIsUnlockedAttribute() {
		return $this->is_open;
	}

	public function getNameReduceAttribute() {
		$name = $this->name;
		if (mb_strlen($name, 'utf-8') >= 50) {
			$name = mb_substr($name, 0, 50, 'utf-8') . "...";
		}
		return $name;
	}

	public function getIsMonthAttribute() {
		$time_frame = $this->time_frame;
		if ($time_frame == 4) {
			return false;
		} else {
			return true;
		};
	}

	public function getIndexInYearAttribute() {
		if ($this->is_month) {
			return Carbon::now()->month - 1;
		} else {
			return Carbon::now()->quarter - 1;
		}
	}

	public function getCurrentTimeTargetAttribute() {
		if ($this->is_month) {
			return $this->getDataTarget()[Carbon::now()->month - 1];
		} else {
			return $this->getDataTarget()[Carbon::now()->quarter - 1];
		}
	}

	public function getDataTarget() {
		$time_frame = $this->time_frame;
		if ($time_frame == 4) {
			return $this->getQRowTarget();
		} else {
			return $this->getMRowTarget();
		};
	}

	public function getDataTargetPadded() {
		$time_frame = $this->time_frame;
		if ($time_frame == 4) {
			$pads = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			return collect($this->getQRowTarget())->union($pads)->toArray();
		} else {
			return $this->getMRowTarget();
		};
	}

	public function getObjectTarget() {
		return $this->targets->pluck('name')->toArray();
	}

	public function getSumTarget() {
		return collect($this->getDataTarget())->sum();
	}

	public function getFormattedStructAttribute() {
		$kpi = $this;
		if ($this->init) {
			$target = $this->getDataTarget();
			$label = $this->getDataLabel();
			$data_target = $this->targets->transform(function ($item) use ($kpi) {
				return [
					'id' => $item->id,
					'object' => $item->name,
					'data' => $item->getData($kpi->id, $kpi->time_frame),
				];
			});
			return [
				'rows_target' => $data_target,
				'target' => $target,
				'label' => $label,
			];
		} else {
			return [];
		}
	}

	public function getDataLabel() {
		$time_frame = $this->time_frame;
		if ($time_frame == 4) {
			return ['Q1', 'Q2', 'Q3', 'Q4'];
		} else {
			return [
				'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
			];
		};
	}

	private function getQRowTarget() {
		$qtarget = $this->qtarget->only(['q1', 'q2', 'q3', 'q4']);
		if (count($qtarget) == 4) {
			return array_flatten($qtarget);
		} else {
			return [-1, -1, -1, -1];
		};
	}

	private function getMRowTarget() {
		$mtarget = $this->mtarget->only(['m1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10', 'm11', 'm12']);
		if (count($mtarget) == 12) {
			return array_flatten($mtarget);
		} else {
			return [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
		};
	}
}
