<?php

namespace App\Models\Kpi;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class KpiRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return Kpi::class;
    }   
}
