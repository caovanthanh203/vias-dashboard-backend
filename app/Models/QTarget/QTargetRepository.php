<?php

namespace App\Models\QTarget;

use Weelis\Repository\Contracts\RepositoryInterface;
use Weelis\Repository\Eloquent\BaseRepository;

class QTargetRepository extends BaseRepository implements RepositoryInterface
{
 	public function model()
    {
        return QTarget::class;
    }   
}
