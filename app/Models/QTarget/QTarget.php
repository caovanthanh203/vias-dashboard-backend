<?php

namespace App\Models\QTarget;

use App\Models\Kpi\Kpi;
use Illuminate\Database\Eloquent\Model;

class QTarget extends Model
{
    protected $fillable = ['q1', 'q2', 'q3', 'q4'];

    public function kpi()
	{
		return $this->belongsTo(Kpi::class);
	}
}
