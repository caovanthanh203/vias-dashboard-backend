<?php
namespace App\Classes;
use App\Models\Schedule\Schedule;

trait Scheduleable
{
	public function schedule()
	{
		return $this->morphOne(Schedule::class, 'scheduleable');
	}

	public function setScheduleStartAtAttribute($value)
	{
		if ($schedule = $this->schedule)
		{
			$schedule->update(['schedule_start_at' => $value]);
		} else {
			$this->schedule()->create(['schedule_start_at' => $value]);
		}
	}

	public function setScheduleEndAtAttribute($value)
	{
		if ($schedule = $this->schedule)
		{
			$schedule->update(['schedule_end_at' => $value]);
		} else {
			$this->schedule()->create(['schedule_end_at' => $value]);
		}
	}
}