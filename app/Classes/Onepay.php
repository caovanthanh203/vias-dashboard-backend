<?php

namespace App\Classes;

class Onepay
{
    public static function do($data, $SECURE_SECRET = "6D0870CDE5F24F34F3915FB0045120DB")
    {
        //extends from OnePay do.php
        $vpcURL = $data["url"] . "?";
        unset($data["url"]); 
        $md5HashData = "";
        ksort ($data);
        $appendAmp = 0;
        foreach($data as $key => $value) {
            if (strlen($value) > 0) {
                if ($appendAmp == 0) {
                    $vpcURL .= urlencode($key) . '=' . urlencode($value);
                    $appendAmp = 1;
                } else {
                    $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                }
                if ((strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
                    $md5HashData .= $key . "=" . $value . "&";
                }
            }
        };
        $md5HashData = rtrim($md5HashData, "&");
        if (strlen($SECURE_SECRET) > 0) {
            $vpcURL .= "&vpc_SecureHash=" . strtoupper(hash_hmac('SHA256', $md5HashData, pack('H*',$SECURE_SECRET)));
        };
        return $vpcURL;
    }

    public static function dr($data, $SECURE_SECRET = "6D0870CDE5F24F34F3915FB0045120DB", $locale = "en"){
        $vpc_Txn_Secure_Hash = Onepay::null2unknown($data, "vpc_SecureHash");
        $vpc_MerchTxnRef = Onepay::null2unknown($data, "vpc_MerchTxnRef");
        $vpc_AcqResponseCode = Onepay::null2unknown($data, "vpc_AcqResponseCode");
        unset($data["vpc_SecureHash"]);
        $errorExists = false;
        if (strlen($SECURE_SECRET) > 0 && Onepay::null2unknown($data, "vpc_TxnResponseCode") != "7" && Onepay::null2unknown($data, "vpc_TxnResponseCode") != "No Value Returned") {
            ksort($data);
            $md5HashData = "";
            foreach ($data as $key => $value) {
                if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
                    $md5HashData .= $key . "=" . $value . "&";
                }
            }
            $md5HashData = rtrim($md5HashData, "&");
            if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper(hash_hmac('SHA256', $md5HashData, pack('H*',$SECURE_SECRET)))) {
                $hashValidated = "CORRECT";
            } else {
                $hashValidated = "INVALID HASH";
            }
        } else {
            $hashValidated = "INVALID HASH";
        }
        $amount = Onepay::null2unknown($data, "vpc_Amount");
        $locale = Onepay::null2unknown($data, "vpc_Locale");
        $batchNo = Onepay::null2unknown($data, "vpc_BatchNo");
        $command = Onepay::null2unknown($data, "vpc_Command");
        $message = Onepay::null2unknown($data, "vpc_Message");
        $version = Onepay::null2unknown($data, "vpc_Version");
        $cardType = Onepay::null2unknown($data, "vpc_Card");
        $orderInfo = Onepay::null2unknown($data, "vpc_OrderInfo");
        $receiptNo = Onepay::null2unknown($data, "vpc_ReceiptNo");
        $merchantID = Onepay::null2unknown($data, "vpc_Merchant");

        $merchTxnRef = Onepay::null2unknown($data, "vpc_MerchTxnRef");
        $transactionNo = Onepay::null2unknown($data, "vpc_TransactionNo");
        $acqResponseCode = Onepay::null2unknown($data, "vpc_AcqResponseCode");
        $txnResponseCode = Onepay::null2unknown($data, "vpc_TxnResponseCode");
        //for 3D security
        $verType = Onepay::null2unknown($data, "vpc_VerType");
        $verStatus = Onepay::null2unknown($data, "vpc_VerStatus");
        $token = Onepay::null2unknown($data, "vpc_VerToken");
        $verSecurLevel = Onepay::null2unknown($data, "vpc_VerSecurityLevel");
        $enrolled = Onepay::null2unknown($data, "vpc_3DSenrolled");
        $xid = Onepay::null2unknown($data, "vpc_3DSXID");
        $acqECI = Onepay::null2unknown($data, "vpc_3DSECI");
        $authStatus = Onepay::null2unknown($data, "vpc_3DSstatus");

        $errorTxt = "";

        if ($txnResponseCode == "7" || $txnResponseCode == "No Value Returned" || $errorExists) {
            $errorTxt = "Error ";
        }

        $title = Onepay::null2unknown($data, "Title");

        $transStatus = "";
        $transBooleanStatus = false;
        if($hashValidated=="CORRECT" && $txnResponseCode=="0"){
            $transStatus = "Giao dịch thành công";
            $transBooleanStatus = true;
        }elseif ($hashValidated=="INVALID HASH" && $txnResponseCode=="0"){
            $transStatus = "Giao dịch Pendding";
        }else {
            $transStatus = "Giao dịch thất bại";
        }
        $message = Onepay::getMessage($txnResponseCode, $locale);
        return compact(['orderInfo', 'transStatus', 'message', 'transBooleanStatus']);
    }

    public static function null2unknown($data, $key)
    {
        if (array_key_exists($key, $data) && !is_null($data[$key])) {
            return $data[$key];
        } else {
            return "No Value Returned";
        }
    }

    public static function getMessage($responseCode, $locale = "en")
    {
        $result = "";
        if ($locale === "en"){
            switch ($responseCode) {
                case "0" :
                $result = "Transaction Successful";
                break;
                case "?" :
                $result = "Transaction status is unknown";
                break;
                case "1" :
                $result = "Bank system reject";
                break;
                case "2" :
                $result = "Bank Declined Transaction";
                break;
                case "3" :
                $result = "No Reply from Bank";
                break;
                case "4" :
                $result = "Expired Card";
                break;
                case "5" :
                $result = "Insufficient funds";
                break;
                case "6" :
                $result = "Error Communicating with Bank";
                break;
                case "7" :
                $result = "Payment Server System Error";
                break;
                case "8" :
                $result = "Transaction Type Not Supported";
                break;
                case "9" :
                $result = "Bank declined transaction (Do not contact Bank)";
                break;
                case "A" :
                $result = "Transaction Aborted";
                break;
                case "C" :
                $result = "Transaction Cancelled";
                break;
                case "D" :
                $result = "Deferred transaction has been received and is awaiting processing";
                break;
                case "F" :
                $result = "3D Secure Authentication failed";
                break;
                case "I" :
                $result = "Card Security Code verification failed";
                break;
                case "L" :
                $result = "Shopping Transaction Locked (Please try the transaction again later)";
                break;
                case "N" :
                $result = "Cardholder is not enrolled in Authentication scheme";
                break;
                case "P" :
                $result = "Transaction has been received by the Payment Adaptor and is being processed";
                break;
                case "R" :
                $result = "Transaction was not processed - Reached limit of retry attempts allowed";
                break;
                case "S" :
                $result = "Duplicate SessionID (OrderInfo)";
                break;
                case "T" :
                $result = "Address Verification Failed";
                break;
                case "U" :
                $result = "Card Security Code Failed";
                break;
                case "V" :
                $result = "Address Verification and Card Security Code Failed";
                break;
                case "99" :
                $result = "User Cancel";
                break;
                default  :
                $result = "Unable to be determined";
            }
        } else {
            switch ($responseCode) {
                case "0" :
                $result = "Giao dịch thành công - Approved";
                break;
                case "1" :
                $result = "Ngân hàng từ chối giao dịch - Bank Declined";
                break;
                case "3" :
                $result = "Mã đơn vị không tồn tại - Merchant not exist";
                break;
                case "4" :
                $result = "Không đúng access code - Invalid access code";
                break;
                case "5" :
                $result = "Số tiền không hợp lệ - Invalid amount";
                break;
                case "6" :
                $result = "Mã tiền tệ không tồn tại - Invalid currency code";
                break;
                case "7" :
                $result = "Lỗi không xác định - Unspecified Failure ";
                break;
                case "8" :
                $result = "Số thẻ không đúng - Invalid card Number";
                break;
                case "9" :
                $result = "Tên chủ thẻ không đúng - Invalid card name";
                break;
                case "10" :
                $result = "Thẻ hết hạn/Thẻ bị khóa - Expired Card";
                break;
                case "11" :
                $result = "Thẻ chưa đăng ký sử dụng dịch vụ - Card Not Registed Service(internet banking)";
                break;
                case "12" :
                $result = "Ngày phát hành/Hết hạn không đúng - Invalid card date";
                break;
                case "13" :
                $result = "Vượt quá hạn mức thanh toán - Exist Amount";
                break;
                case "21" :
                $result = "Số tiền không đủ để thanh toán - Insufficient fund";
                break;
                case "99" :
                $result = "Người sủ dụng hủy giao dịch - User cancel";
                break;
                default :
                $result = "Giao dịch thất bại - Failured";
            }
        }
        return $result;
    }
}