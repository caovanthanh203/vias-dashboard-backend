<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'shield'], function () {
	Route::get('/', function(){return view('app');});
	Route::get('/handler', 'DashboardController@home')->name('home');

	Route::get('/helper/gen-otp', 'Controller@genOtp');
	
	Route::get('/dashboard/data', 'DashboardController@data')->name('admin.dashboard.data');
	Route::get('/dashboard/setting', 'DashboardController@settingList')->name('admin.dashboard.setting.list');
	Route::post('/dashboard/setting', 'DashboardController@settingStore')->name('admin.dashboard.setting.store');
	
	Route::get('/company/list', 'CompanyController@index')->name('admin.company.list');
	Route::post('/company/store', 'CompanyController@store')->name('admin.company.store');
	Route::post('/company/update', 'CompanyController@update')->name('admin.company.update');
	Route::get('/company/employees/list/{id}', 'CompanyController@employees')->name('admin.company.employees.list');
	Route::get('/company/employees/detail/{id}', 'CompanyController@detailemployees')->name('admin.company.employees.detail');
	Route::post('/company/employees/store', 'CompanyController@storeemployees')->name('admin.company.employees.store');
	Route::post('/company/employees/update', 'CompanyController@updateemployees')->name('admin.company.employees.update');
	Route::post('/company/delete', 'CompanyController@delete')->name('admin.company.delete');
	
	Route::get('/department/list', 'DepartmentController@index')->name('admin.department.list');
	Route::post('/department/store', 'DepartmentController@store')->name('admin.department.store');
	Route::post('/department/delete', 'DepartmentController@delete')->name('admin.department.delete');
	Route::get('/department/select', 'DepartmentController@select')->name('admin.department.select');
	Route::get('/department/suggestions', 'DepartmentController@suggestions');
	Route::get('/department/option', 'DepartmentController@option');
	
	Route::get('/kpi/list', 'KpiController@index')->name('admin.kpi.list');
	Route::get('/kpi/list-all', 'KpiController@list')->name('admin.kpi.list.all');
	Route::post('/kpi/store', 'KpiController@store')->name('admin.kpi.store');
	Route::get('/kpi/edit/{id}', 'KpiController@edit');
	Route::post('/kpi/update', 'KpiController@update');
	Route::post('/kpi/save', 'KpiController@save');
	Route::post('/kpi/unlock', 'KpiController@changeStatus');
	Route::get('/kpi/show/{id}', 'KpiController@show');
	Route::post('/kpi/init', 'KpiController@init');
	Route::post('/kpi/re-init', 'KpiController@reInit');
	Route::get('/kpi/edit-target', 'KpiController@editTarget')->name('admin.kpi.target.edit');
	Route::post('/kpi/update-target', 'KpiController@updateTarget')->name('admin.kpi.target.update');
	Route::post('/kpi/delete', 'KpiController@delete')->name('admin.kpi.delete');
	Route::get('/kpi/report-list', 'KpiController@reportList')->name('admin.kpi.report.list');
	Route::get('/kpi/select', 'KpiController@select')->name('admin.kpi.select');
	
	Route::get('/employees/departments', 'EmployeesController@department')->name('admin.employees.department');
	Route::get('/employees/kpi', 'EmployeesController@kpi')->name('admin.employees.kpi');
	Route::get('/employees/list', 'EmployeesController@index')->name('admin.employees.list');
	Route::get('/employees/option', 'EmployeesController@option');
	Route::get('/employees/detail/{id}', 'EmployeesController@detail')->name('admin.employees.detail');
	Route::post('/employees/delete', 'EmployeesController@destroy')->name('admin.employees.delete');
	Route::post('/employees/store', 'EmployeesController@store')->name('admin.employees.store');
	Route::post('/organization/store', 'EmployeesController@store');
	Route::post('/employees/update', 'EmployeesController@update')->name('admin.employees.update');
	
	Route::get('/charttype/list', 'ChartTypeController@index')->name('admin.charttype.list');
	
	Route::get('/location/select', 'LocationController@select');

	Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'guest'], function () {
	Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
	Route::post('login', 'Auth\LoginController@login')->name('admin.login');
	Route::get('reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.reset');
});


