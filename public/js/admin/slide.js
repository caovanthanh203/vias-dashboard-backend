$(document).ready(function() {
  $(".one-slick").slick({
    infinite: true,
    autoplay: true,
    prevArrow:
      '<button type="button" class="slick-prev-cus"><i class="ti-angle-left"></i></button>',
    nextArrow:
      '<button type="button" class="slick-next-cus"><i class="ti-angle-right"></i></button>'
  });
});
