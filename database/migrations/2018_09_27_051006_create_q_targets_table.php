<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('q_targets', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedBigInteger('kpi_id')->nullable()->index();
            $table->unsignedInteger('q1')->nullable();
            $table->unsignedInteger('q2')->nullable();
            $table->unsignedInteger('q3')->nullable();
            $table->unsignedInteger('q4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('q_targets');
    }
}
