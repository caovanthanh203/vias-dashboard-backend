<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('q_rows', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->unsignedBigInteger('qrowable_id')->nullable()->index();
            $table->string('qrowable_type')->nullable()->index();
            $table->unsignedBigInteger('kpi_id')->nullable()->index();
            $table->unsignedInteger('q1')->nullable();
            $table->unsignedInteger('q2')->nullable();
            $table->unsignedInteger('q3')->nullable();
            $table->unsignedInteger('q4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('q_rows');
    }
}
