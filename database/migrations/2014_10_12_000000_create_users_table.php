<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('code')->unique()->index();
            $table->Integer('department_id')->index();
            $table->unsignedBigInteger('company_id')->default(0)->index();
            $table->string('name')->index();
            $table->string('otp')->nullable();
            //$table->string('email')->unique();
            //$table->string('phone')->index()->nullable();
            $table->string('password');
            $table->longText('home_kpi')->nullable();
            $table->boolean('init')->default(false)->index();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
