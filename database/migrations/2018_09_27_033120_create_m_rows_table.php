<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_rows', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedBigInteger('mrowable_id')->nullable()->index();
            $table->string('mrowable_type')->nullable()->index();
            $table->unsignedBigInteger('kpi_id')->nullable()->index();
            $table->unsignedInteger('m1')->nullable();
            $table->unsignedInteger('m2')->nullable();
            $table->unsignedInteger('m3')->nullable();
            $table->unsignedInteger('m4')->nullable();
            $table->unsignedInteger('m5')->nullable();
            $table->unsignedInteger('m6')->nullable();
            $table->unsignedInteger('m7')->nullable();
            $table->unsignedInteger('m8')->nullable();
            $table->unsignedInteger('m9')->nullable();
            $table->unsignedInteger('m10')->nullable();
            $table->unsignedInteger('m11')->nullable();
            $table->unsignedInteger('m12')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_rows');
    }
}
