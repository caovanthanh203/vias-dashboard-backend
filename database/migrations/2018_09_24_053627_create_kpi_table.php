<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpi', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->unsignedBigInteger('chart_type_id')->nullable();
            $table->unsignedInteger('time_frame')->nullable();
            $table->unsignedBigInteger('company_id')->default(0)->index();
            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->unsignedBigInteger('creator_id')->nullable()->index();
            $table->unsignedBigInteger('department_id')->default(0)->index();
            $table->string('code')->unique()->index();
            $table->string('name')->index();
            $table->string('unit')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('init')->default(false)->index();
            $table->boolean('is_open')->default(false)->index();
            $table->timestamps();
        });
        Schema::create('target_kpi', function (Blueprint $table) {
            $table->unsignedBigInteger('target_id')->index();
            $table->unsignedBigInteger('kpi_id')->index();
            $table->unique(["target_id", "kpi_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpi');
        Schema::dropIfExists('target_kpi');
    }
}
