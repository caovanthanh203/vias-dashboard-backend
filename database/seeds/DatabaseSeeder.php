<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ChartTypesTableSeeder::class);
        $this->call(KpiTableSeeder::class);
        $this->call(LocationsTableSeeder::class);        
    }
}
