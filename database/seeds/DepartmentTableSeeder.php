<?php

use Illuminate\Database\Seeder;
use App\Models\Department\Department;
class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Nhân Sự',
            'Kế Toán',
            'Sản Xuất',
            'Quy Trình',
            'Kỹ Thuật',
            'Mua Hàng',
            'Cung Ứng',
            'Vật Tư',
            'Chất Lượng',
            'An Toàn & Môi Trường',
            'Dự Án',
            'Kế Hoạch',
            'Nghiên Cứu',
            'Bảo Trì'
        ];
		foreach ($names as $index => $name) {
			$item = 
			[
			"name"    		=> $name,
            'company_id'    => 1,
			];
			$department = Department::create($item);
		};
    }
}
