<?php

use Illuminate\Database\Seeder;
use App\Models\Role\Role;
use App\Models\User\User;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$admin = [
					"name"      	=> 'S-Admin',
					"code"     		=> 'superadmin',
					"password"  	=> bcrypt('12345678'),
					'department_id'	=> 0
				];
		User::create($admin)->roles()->attach([5]);

		$rooms = [
			[
				'code' => '13011234',	
				'name' => 'Trần Thành Nam', 
				'department_id' => 1, 
				'company_id' => 1, 
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011248',	
				'name' => 'Dương Trung Nghĩa'	, 
				'department_id' => 11	, 
				'company_id' => 1, 
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			]
		];
		foreach ($rooms as $room) {
			User::create($room)->roles()->attach([3]);
		}

		$staffs = [
			[
				'code' => '13011237',	
				'name' => 'Nguyễn Văn Định'		, 
				'department_id' => 1	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011241',	
				'name' => 'Lê Thị Hằng'			, 
				'department_id' => 1	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011245',	
				'name' => 'Nguyễn Ngọc Quyên'	, 
				'department_id' => 1	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011246',	
				'name' => 'Bùi Thị Diệp'		, 
				'department_id' => 1	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011252',	
				'name' => 'Trần Hải Quân'		, 
				'department_id' => 11	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011257',	
				'name' => 'Lê Văn Hưu'			, 
				'department_id' => 11	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011249',	
				'name' => 'Mã Thành Hiệp'		, 
				'department_id' => 11	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011272',	
				'name' => 'Lưu Thị Bích Loan'	, 
				'department_id' => 11	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011273',	
				'name' => 'Trần Trọng'			, 
				'department_id' => 11	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011274',	
				'name' => 'Huỳnh Hoàng Huy'		, 
				'department_id' => 11	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			],
			[
				'code' => '13011275',	
				'name' => 'Trần Tiến Luật'		, 
				'department_id' => 11	, 
				'company_id' => 1,
				"password" => bcrypt('12345678'), 
				'otp' => '12345678'
			]
		];
		foreach ($staffs as $staff) {
			User::create($staff)->roles()->attach([4]);
		}
	}
}
