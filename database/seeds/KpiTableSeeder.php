<?php

use Illuminate\Database\Seeder;
use App\Models\Kpi\Kpi;

class KpiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kpis_name = [
            'Chỉ số tiêu thụ Điện',
            'Chỉ số tiêu thụ Nước',
            'Chỉ số tiêu thụ Giấy',
            'Số nhân viên nghỉ việc',
            'Số giờ tăng ca của nhân viên',
            'Số giờ đào tạo nhân viên',
            'Số giờ đào tạo An toàn cho nhà thầu',
            'Phản hồi nhân viên về Lương',
            'Tỉ lệ trả lương đúng ngày',
            'Số nhân viên mới tuyển dụng',
            'Phản hồi nhân viên về BHXH',
        ];
        $kpis_user = [4, 4, 4, 7, 7, 5, 5, 5, 6, 6, 6];
        foreach ($kpis_name as $index => $value) {
            $kpi = [
                'name'          => $value,
                'code'          => "KPI_CODE_{$index}",
                'company_id'    => 1,
                'department_id' => 1,
                'user_id'       => $kpis_user[$index]
            ];
            $kpi = Kpi::create($kpi);
        }
    }
}
