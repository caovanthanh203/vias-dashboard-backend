<?php

use Illuminate\Database\Seeder;
use App\Models\Company\Company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = 
		[
		'name'          => 'Công ty Thực Phẩm Sài Gòn',
        'short_name'    => 'SGF',
        'location_id'   => 79
		];
		$company = Company::create($item);
    }
}
