<?php

use Illuminate\Database\Seeder;
use App\Models\Role\Role;

class RoleTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$roles = [
			[
				"id" => 1, "name" => title_case('Admin'), "slug" => str_slug('admin')
			],
			[
				"id" => 2, "name" => title_case('Giám đốc'), "slug" => str_slug('manager')
			],
			[
				"id" => 3, "name" => title_case('Trưởng phòng'), "slug" => str_slug('generalinchief')
			],
			[
				"id" => 4, "name" => title_case('Nhân viên'), "slug" => str_slug('staff')
			],
			[
				"id" => 5, "name" => title_case('SuperAdmin'), "slug" => str_slug('superadmin')
			],
		];
		foreach ($roles as $role) {
			Role::create($role);
		}
	}
}
