<?php

use Illuminate\Database\Seeder;
use App\Models\ChartType\ChartType;
class ChartTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items =
			[
				[
					"name"      => 'type-bar',
					"icon"     	=> '../../assets/images/chart/chart-bar.png'
				],
				[
					"name"      => 'type-bar-stacked',
					"icon"     	=> '../../assets/images/chart/chart-stacked.png'
				],
				[
					"name"      => 'type-line',
					"icon"     	=> '../../assets/images/chart/chart-line.png'
				],
				[
					"name"      => 'type-pie',
					"icon"     => '../../assets/images/chart/chart-pie.png'
				],
				[
					"name"      => 'type-combo',
					"icon"     => '../../assets/images/chart/chart-combo.png'
				],
				[
					"name"      => 'type-radar',
					"icon"     => '../../assets/images/chart/chart-radar.png'
				]
			];
		foreach ($items as $item) {
			ChartType::create($item);
		}
    }
}
