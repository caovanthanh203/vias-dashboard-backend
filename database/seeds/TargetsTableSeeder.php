<?php

use Illuminate\Database\Seeder;
use App\Models\Target\Target;

class TargetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
		for ($i = 1; $i <= 50; $i++){
			$item = 
			[
			"name"    		=> "Đối Tượng {$i}"
			];
			$target = Target::create($item);
		};
    }
}
