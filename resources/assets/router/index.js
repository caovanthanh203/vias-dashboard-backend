import Vue from 'vue'
import Router from 'vue-router'
//import VueRouter from 'vue-router'
import Dashboard from './../components/dashboard/dashboard'
import LineView from './../components/dashboard/line'
import ChartView from './../components/dashboard/chart'
import SpeedView from './../components/dashboard/speed'
import Organizations from './../components/employees/list'
import Department from './../components/department/list'
import Kpi from './../components/kpi/list'
import InitKpi from './../components/kpi/init'
import EditKpi from './../components/kpi/edit'
import Report from './../components/report/report'
import ReportList from './../components/report/list'
import ReportImport from './../components/report/import'
import Company from './../components/company/list'
import Acl from 'vue-browser-acl'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Dashboard,
      name: Dashboard.name,
      icon: Dashboard.icon,
      children: [
        {
          path: "line-view",
          component: LineView,
          name: LineView.name,
          viewName: LineView.viewName,
          icon: LineView.icon,
          bg: LineView.bg
        },
        {
          path: "chart-view",
          component: ChartView,
          name: ChartView.name,
          viewName: ChartView.viewName,
          icon: ChartView.icon,
          bg: ChartView.bg
        }
        ,
        // {
        //   path: "speed-view",
        //   component: SpeedView,
        //   name: SpeedView.name,
        //   viewName: SpeedView.viewName,
        //   icon: SpeedView.icon,
        //   bg: SpeedView.bg
        // }
      ]
    },
    {
      path: '/report',
      component: Report,
      name: Report.name,
      icon: ReportList.icon,
      children: [
        {
          path: "list",
          component: ReportList,
          name: ReportList.name,
        },
        {
          path: "import/:id",
          component: ReportImport,
          name: ReportList.name,

        }
      ]
    },
    {
      path: '/kpi',
      component: Kpi,
      name: Kpi.name,
      icon: Kpi.icon

    },
    {
      path: '/kpi/init/:id',
      component: InitKpi,
      name: InitKpi.name,
      icon: InitKpi.icon

    },
    {
      path: '/kpi/edit/:id',
      component: EditKpi,
      name: EditKpi.name,
      icon: EditKpi.icon

    },
    {
      path: '/organizations',
      component: Organizations,
      name: Organizations.name,
      icon: Organizations.icon
    },
    // {
    //   path: '/deparment',
    //   component: Department,
    //   name: Department.name,
    //   icon: Department.icon
    // }, 
    {
      path: '/company',
      component: Company,
      name: Company.name,
      icon: Company.icon,
    }
    
  ]
})



let role = document.head.querySelector('meta[name="role"]').content;

Vue.use(Acl, role, (acl) => {
    acl.rule(['list'], 'Company', (role) => {
        return (role === "superadmin")
    })
    acl.rule(['create'], 'Company', (role) => {
        return (role === "superadmin")
    })


    acl.rule(['create','delete'], 'Organizations', (role) => {
        return (role === "admin" || role === "generalinchief")
    })
    acl.rule(['edit'], 'Organizations', (role) => {
        return (role === "admin" || role === "generalinchief")
    })
    acl.rule(['edit'], 'DetailEmployees', (role) => {
        return (role === "admin" || role === "generalinchief" || role === "manager")
    })
    acl.rule(['edit'], 'KpiEmployees', (role) => {
        return (role === "generalinchief"|| role === "manager")
    })
    acl.rule(['list'], 'Organizations', (role) => {
        return (role === "admin" || role === "generalinchief" || role === "manager")
    })
    acl.rule(['read'], 'Organizations', (role) => {
        return (role === "manager" || role === "admin" || role === "generalinchief")
    })


    acl.rule(['create', 'edit'], 'KPI', (role) => {
        return (role === "generalinchief" || role === "staff")
    })
    acl.rule(['read'], 'KPI', (role) => {
        return (role === "manager")
    })
    acl.rule(['delete'], 'KPI', (role) => {
        return (role === "generalinchief")
    })
    acl.rule('list', 'KPI', (role) => {
        return (role === "generalinchief" || role === "manager")
    })
    acl.rule(['list'], 'Report', (role) => {
        return (role === "staff" || role === "generalinchief" || role === "manager")
    })
    acl.rule('create', 'NotManager', (role) => {
        return (role !== "manager")
    })
    acl.rule('alone', 'IsManager', (role) => {
        return (role === "manager")
    })
    acl.rule('alone', 'IsManagerAdmin', (role) => {
        return (role === "manager" || role === "admin")
    })
    acl.rule('alone', 'IsGeneralinchief', (role) => {
        return (role === "generalinchief")
    })
    acl.rule('alone', 'NotGeneralinchief', (role) => {
        return ((role === "manager" || role === "admin") && role !== "generalinchief")
    })
    acl.rule('alone', 'IsSuperAdmin', (role) => {
        return (role === "superadmin")
    })
    acl.rule('alone', 'NotSuperAdmin', (role) => {
        return (role !== "superadmin")
    })
    acl.rule('alone', 'IsGeneralinchiefStaff', (role) => {
        return (role === "staff" || role === "generalinchief")
    })
    acl.rule('alone', 'IsAdmin', (role) => {
        return (role === "admin")
    })
    acl.rule('alone', 'NotAdmin', (role) => {
        return (role !== "admin")
    })
})
