/**
 * Created by simonnguyen on 8/11/17.
 */
const CustomUrl = {
    install(Vue, options) {
        // 1. add global method or property
        Vue.url = function (path) {
            if (path) {
                return options.baseURL + path
            }
            return options.baseURL
        }
        // 4. add an instance method
        Vue.prototype.$url = function (path) {
            if (path) {
                return options.baseURL + path
            }
            return options.baseURL
        }

        Vue.prototype.$encodedUrl = function (path) {
            if (path) {
                return encodeURI(options.baseURL + path)
            }
            return encodeURI(options.baseURL)
        }
    }
}

export default CustomUrl;