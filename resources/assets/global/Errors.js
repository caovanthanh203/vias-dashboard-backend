class Errors {
    constructor() {
        this.errors = {};
        this.message = '';
    }
    has(field) {
        return this.errors.hasOwnProperty(field);
    }
    any() {
        return Object.keys(this.errors).length > 0;
    }
    all() {
        return this.errors;
    }
    first(field){
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }
    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }
    getMessage() {
        return this.message;
    }
    record(errors) {
        if(errors.hasOwnProperty('errors')) {
            this.errors = errors['errors'];
        } else {
            this.errors = errors;
        }
        this.message = errors['message'];
    }
    clear(field) {
        if (field) {
            delete this.errors[field];

            return;
        }

        this.errors = {};
        this.message = '';
    }
}

export default Errors;