import Errors from './Errors';

var objectToFormData = function (obj, fields, form, namespace) {

    var fd = form || new FormData();
    var formKey;
    var hasProperties = false;

    for (var property in fields) {
        if (obj.hasOwnProperty(property)) {
            if (!(obj[property] != null && obj[property] != '')) {
                continue;
            }
            if (namespace) {
                formKey = namespace + '[' + property + ']';
            } else {
                formKey = property;
            }
            // if the property is an object, but not a File,
            // use recursivity.
            // if (obj[property] && obj[property].constructor === Array) {
            //     objectToFormData(obj[property], fields[property], fd, formKey);
            // }
            // else
            if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                objectToFormData(obj[property], fields[property], fd, formKey);
            } else {
                if (typeof(obj[property]) === "boolean") {
                    fd.append(formKey, (typeof(obj[property]) == typeof(true)) ? 1 : 0);
                } else {
                    // if it's a string or a File object
                    fd.append(formKey, obj[property]);
                }
            }
        }
        hasProperties = true
    }
    if (!hasProperties) {
        if (obj && obj.constructor === Array) {
            obj.forEach(function (item, index) {
                if (typeof item === 'object' && !(item instanceof File)) {
                    objectToFormData(item, item, fd, namespace + "[" + index + "]");
                } else {
                    fd.append(namespace + "[]", item);
                }
            })
        } else {
            fd.append(namespace, obj);
        }
    }

    return fd;
};

class Form {
    constructor(data) {
        this.originalData = data
        for (var field in data) {
            this[field] = data[field]
        }
        this.errors = new Errors()
    }

    formData() {
        if (this.originalData.hasOwnProperty("data")) {
            return objectToFormData(this["data"], this.originalData["data"], null, "data");
        }
        return objectToFormData(this, this.originalData);
    }

    reset() {
        for (var field in this.originalData) {
            this[field] = ''
        }

        this.errors.clear();
    }

    get(url) {
        return this.submit('get', url)
    }

    post(url) {
        return this.submit('post', url)
    }

    put(url) {
        return this.submit('put', url)
    }

    patch(url) {
        return this.submit('patch', url)
    }

    delete(url) {
        return this.submit('delete', url)
    }

    submit(requestType, url) {
        var _ = this
        _.loading = true
        return new Promise((resolve, reject) => {
            axios({
                method: requestType,
                url: url.replace(/(&?\w+=((?=$)|(?=&)))/g, ''),
                data: _.formData()
            }).then(function (response) {
                _.onSuccess(response.data)
                _.loading = false
                resolve(response.data)
            })
                .catch(function (error) {
                    if (error.response) {
                        if (error.response.status == 401) {
                            location.href = "/"
                            return
                        }
                        if (error.response.status != 422) {
                            reject(error);
                            _.loading = false
                            return
                        }
                        _.onFail(error.response.data)
                    } else {
                        reject(error)
                    }
                    _.loading = false
                })
        })
    }

    onSuccess(data) {
        // this.reset()
        if (data.constructor === Array) {
            this.data = data
        } else {
            for (var field in data) {
                this[field] = data[field]
            }
        }
    }

    onFail(errors) {
        this.errors.record(errors)
    }

    isLoading() {
        return this.loading
    }

    hasError(key) {
        return this.errors.has(key)
    }

    showError(key) {
        return this.errors.first(key)
    }

    showErrorMesage() {
        return this.errors.getMessage()
    }

    allError() {
        return this.errors.all()
    }

    anyError() {
        return this.errors.any()
    }
}

export default Form