// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import testData from "./assets/js/data.json";
import CustomUrl from './global/Url';

Vue.config.productionTip = false;
let url = document.head.querySelector('meta[name="url"]');
window.baseURL = url.content;


require("./assets/plugin/jstree/style.css");
require("./assets/plugin/nestable/jquery.nestable.css");

require("./assets/css/bootstrap.min.css");
require("./assets/css/icons.css");
require("./assets/css/style.css");
require("./assets/css/vias-custom.css");
window.FastClick = require("fastclick");
window.$ = window.jQuery = require("jquery");
require("popper.js");
require("bootstrap");
require("./assets/js/detect");
require("jquery-slimscroll");
require("block-ui");
require("./assets/js/waves");
window.WOW = require("wow.js");
require("jquery.nicescroll");
require("jquery.scrollto");
require("chart.js");
require("jstree");
require("peity");
window.zingchart = require("zingchart");
require("nestable");

require("./assets/js/jquery.core");
window.axios = require('axios');
window.axios.defaults.baseURL = url.content;
Vue.use(CustomUrl, {baseURL: baseURL});
window.swalcustom = require("sweetalert");
window.swal = require("sweetalert2");
require("select2/dist/css/select2.min.css");
require("select2/dist/js/select2.full.min.js");
require("v-suggestions");

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  data() {
    return {
      myData: testData,
    }
  },
  mounted() {
    $("body").append('<script src="./static/jquery.app.js"></script>');
  },
  methods: {
    name: function() {
        let check = document.head.querySelector('meta[name="name"]');
        return (check.content);
    },
    role: function() {
        let check = document.head.querySelector('meta[name="role"]');
        return check.content;
    },
    role_name: function() {
        let check = document.head.querySelector('meta[name="role_name"]');
        return check.content;
    },
    startLoading() {
            $(".preloader").show();
            setTimeout(function(){
            $(".preloader")
            .delay(300)
            .fadeOut();
          },5000);
        },
    stopLoading() {
      console.log('stopLoading');
            $(".preloader")
            .delay(300)
            .fadeOut();
    }
  }
})
