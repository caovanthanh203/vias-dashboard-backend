<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    'uniqueby3cols'        => 'Trường :attribute đã có trong cơ sở dữ liệu.',
    'uniquebycols'         => 'Trường :attribute đã có trong cơ sở dữ liệu.',
    'accepted'             => 'Trường :attribute phải được chấp nhận.',
    'active_url'           => 'Trường :attribute không phải là một URL hợp lệ.',
    'after'                => 'Trường :attribute phải là một ngày sau ngày :date.',
    'after_or_equal'       => 'Trường :attribute phải là thời gian bắt đầu sau :date.',
    'alpha'                => 'Trường <strong>:attribute</strong> chỉ có thể chứa các chữ cái.',
    'alpha_dash'           => 'Trường <strong>:attribute</strong> chỉ có thể chứa chữ cái, số và dấu gạch ngang.',
    'alpha_num'            => 'Trường :attribute chỉ có thể chứa chữ cái và số.',
    'array'                => 'Trường :attribute phải là dạng mảng.',
    'before'               => 'Trường :attribute phải là một ngày trước ngày :date.',
    'before_or_equal'      => 'Trường :attribute phải là thời gian bắt đầu trước :date.',
    'between'              => [
        'numeric' => 'Trường :attribute phải nằm trong khoảng :min - :max.',
        'file'    => 'Dung lượng tập tin trong trường :attribute phải từ :min - :max kB.',
        'string'  => 'Trường :attribute phải từ :min - :max ký tự.',
        'array'   => 'Trường :attribute phải có từ :min - :max phần tử.',
    ],
    'boolean'              => 'Trường :attribute phải là true hoặc false.',
    'confirmed'            => 'Giá trị xác nhận trong trường :attribute không khớp.',
    'date'                 => 'Trường :attribute không phải là định dạng của ngày-tháng.',
    'date_format'          => 'Trường :attribute không giống với định dạng :format.',
    'different'            => 'Trường :attribute và :other phải khác nhau.',
    'digits'               => 'Độ dài của trường :attribute phải gồm :digits chữ số.',
    'digits_between'       => 'Độ dài của trường :attribute phải nằm trong khoảng :min and :max chữ số.',
    'dimensions'           => 'Trường :attribute có kích thước không hợp lệ.',
    'distinct'             => 'Trường :attribute có giá trị trùng lặp.',
    'email'                => 'Trường :attribute phải là một địa chỉ email hợp lệ.',
    'exists'               => ':attribute không tồn tại.',
    'file'                 => 'Trường :attribute phải là một tệp tin.',
    'filled'               => 'Trường :attribute không được bỏ trống.',
    'image'                => 'Trường :attribute phải là định dạng hình ảnh.',
    'in'                   => 'Giá trị đã chọn trong trường :attribute không hợp lệ.',
    'in_array'             => 'Trường :attribute phải thuộc tập cho phép: :other.',
    'integer'              => 'Trường :attribute phải là một số nguyên.',
    'ip'                   => 'Trường :attribute phải là một địa chỉ IP.',
    'ipv4'                 => 'Trường :attribute phải là một địa chỉ IPv4.',
    'ipv6'                 => 'Trường :attribute phải là một địa chỉ IPv6.',
    'json'                 => 'Trường :attribute phải là một chuỗi JSON.',
    'max'                  => [
        'numeric' => 'Trường :attribute không được lớn hơn :max.',
        'file'    => 'Dung lượng tập tin trong trường :attribute không được lớn hơn :max kB.',
        'string'  => 'Trường :attribute không được lớn hơn :max ký tự.',
        'array'   => 'Trường :attribute không được lớn hơn :max phần tử.',
    ],
    'mimes'                => 'Trường :attribute phải là một tập tin có định dạng: :values.',
    'mimetypes'            => 'Trường :attribute phải là một tập tin có định dạng: :values.',
    'min'                  => [
        'numeric' => 'Trường :attribute phải tối thiểu là :min.',
        'file'    => 'Dung lượng tập tin trong trường :attribute phải tối thiểu :min kB.',
        'string'  => 'Trường :attribute phải có tối thiểu :min ký tự.',
        'array'   => 'Trường :attribute phải có tối thiểu :min phần tử.',
    ],
    'not_in'               => 'Giá trị đã chọn trong trường :attribute không hợp lệ.',
    'numeric'              => 'Trường :attribute phải là một số.',
    'present'              => 'Trường :attribute phải được cung cấp.',
    'slug'                 => 'Định dạng trường :attribute không hợp lệ.',
    'phone'                => 'Định dạng trường :attribute không hợp lệ.',
    'array_val'            => 'Định dạng trường :attribute không hợp lệ.',
    'regex'                => 'Định dạng trường :attribute không hợp lệ.',
    'required'             => 'Trường <strong>:attribute</strong> không được bỏ trống.',
    'required_if'          => 'Trường :attribute không được bỏ trống khi trường :other là :value.',
    'required_unless'      => 'Trường :attribute không được bỏ trống trừ khi :other là :values.',
    'required_with'        => 'Trường :attribute không được bỏ trống khi một trong :values có giá trị.',
    'required_with_all'    => 'Trường :attribute không được bỏ trống khi tất cả :values có giá trị.',
    'required_without'     => 'Trường :attribute không được bỏ trống khi một trong :values không có giá trị.',
    'required_without_all' => 'Trường :attribute không được bỏ trống khi tất cả :values không có giá trị.',
    'same'                 => 'Trường :attribute và :other phải giống nhau.',
    'size'                 => [
        'numeric' => 'Trường :attribute phải bằng :size.',
        'file'    => 'Dung lượng tập tin trong trường :attribute phải bằng :size kB.',
        'string'  => 'Trường :attribute phải chứa :size ký tự.',
        'array'   => 'Trường :attribute phải chứa :size phần tử.',
    ],
    'string'               => 'Trường :attribute phải là một chuỗi ký tự.',
    'timezone'             => 'Trường :attribute phải là một múi giờ hợp lệ.',
    'unique'               => 'Trường <strong>:attribute</strong> đã có trong cơ sở dữ liệu.',
    'uploaded'             => 'Trường :attribute tải lên thất bại.',
    'url'                  => 'Trường :attribute không giống với định dạng một URL.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        "*.name"                => "họ và tên",
        "*.phone"               => "số điện thoại",
        "phone"                 => "số điện thoại",
        "*.email"               => "địa chỉ email",
        "email"                 => "địa chỉ email",
        "*.gender"              => "giới tính",
        "gender"                => "giới tính",
        "*.house_id"            => "căn hộ",
        "house_id"              => "căn hộ",
        "house_ids"             => "căn hộ",
        "*.public_start"        => "ngày gửi",
        "public_start"          => "ngày gửi",
        "*.public_end"          => "Ngày kết thúc",
        "public_end"            => "Ngày kết thúc",
        "*.license_plate"       => "Biển số xe",
        "license_plate"         => "Biển số xe",
        "resident_id"           => "cư dân",
        "survey_file"           => "File đính kèm",
        "ap_subdomain"          => "subdomain",
        "id_code"               => "số chứng minh",
        "permanent_address"     => "địa chỉ thường trú",
        "birth_day"             => "ngày sinh",
        "date_off_issue"        => "ngày cấp",
        "issued_by"             => "nơi cấp",
        "ap_cover"              => "hình đại diện",
        "contact_name"          => "họ và tên",
        "contact_phone"         => "số điện thoại",
        "contact_email"         => "email",
        "name"                  => "tên",
        "title"                 => "tiêu đề",
        "survey_content"        => "nội dung",
        "targets"               => "đối tượng",
        "block_id"              => "block",
        "floor_id"              => "tầng",
        "housetype_id"          => "loại căn hộ",
        "house_way"             => "hướng căn hộ",
        "area_of_use"           => "diện tích sử dụng",
        "construction_area"     => "diện tích xây dựng",
        "file"                  => "tập tin",
        "*.type_id"             => "loại",
        "type_id"               => "loại",
        "vehicle_id.*"          => "xe",
        "is_owner"              => "chức danh",
        "scope"                 => "phạm vi",
        "searchKey"             => "chuỗi tìm kiếm",
        "status"                => "tình trạng",
        "invoice_id.*"          => "danh sách hoá đơn",
        "resident.name"         => "họ và tên",
        "resident.gender"       => "giới tính",
        "resident.phone"        => "điện thoại",
        "resident.email"        => "địa chỉ email",
        "data.*.name"           => "họ và tên",
        "data.*.gender"         => "giới tính",
        "data.*.phone"          => "điện thoại",
        "data.*.email"          => "địa chỉ email",
        "data.*.house_ids"      => "căn hộ",
        "options.*.name"        => "tên tuỳ chọn",
        "reject_comment"        => "lý do từ chối",
        "price"                 => "giá tiền",
        "current_password"      => "mật khẩu hiện tại",
        "password"              => "mật khẩu",
        "password_confirmation" => "xác nhận mật khẩu",
        "user_id.*"             => "tài khoản",
        "role_id"               => "chức danh",
        "role"                  => "chức danh",
        "data.*.house_id"       => "Căn hộ",
        "house_id"              => "Căn hộ",
        "ap_status"             => "tình trạng chung cư",
        "ap_lat"                => "vĩ độ",
        "ap_lng"                => "kinh độ",
        "ap_name"               => "tên chung cư",
        "ap_area"               => "diện tích",
        "ap_address"            => "địa chỉ",
        "investor_logo"         => "logo",
        "investor_name"         => "tên chủ đầu tư",
        "investor_website"      => "website chủ đầu tư",
        "floor_number"          => "số tầng",
        'data.*.type_id'        => 'loại xe',
        'data.*.resident_id'    => 'cư dân',
        'data.*.license_plate'  => 'biển số xe',
        'data.*.public_start'   => 'ngày gửi',
        'answer'                => 'câu trả lời',
        'answer_note'           => 'câu trả lời',
        'document_file'         => 'tập tin tài liệu',
        'comment'               => 'ghi chú',
        'today'                 => 'hôm nay',
        'content_invoice'       => 'nội dung hóa đơn',
        'complaint_file'       => 'hình ảnh đính kèm',
        'invoice_exported'      => 'ngày phát hành',
        'invoice_expired'       => 'ngày hết hạn',
        //thanh doing
        'company_name'              => 'tên công ty',
        'company_short_name'        => 'tên viêt tắt',
        'company_location'          => 'vị trí',
        'admin_id'                  => 'ID admin',
        'admin_name'                => 'họ tên admin',
        'person_name'               => 'họ tên',
        'person_code'               => 'ID',
        'person_department_name'    => 'tên phòng ban',
        'person_role'               => 'chức vụ',
        'kpi_code'                  => 'mã số KPI',
        'kpi_name'                  => 'tên KPI',
        'kpi_owner'                 => 'người chịu trách nhiệm',
        'kpi_unit'                  => 'đơn vị',
        'kpi_description'           => 'mô tả KPI-công thức tính',
        'kpi_target'                => 'đối tượng theo dõi'
    ],

];
