<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'company'   =>  [
        'success'   =>  [
            'created'   => 'Công ty :name đã được thêm thành công!',
            'updated'   => 'Công ty :name đã được cập nhật thành công!',
            'deleted'   => 'Công ty :name đã được xóa thành công!'
        ]
    ],
    'organization'   =>  [
        'success'   =>  [
            'created'   => 'Người dùng :name đã được thêm thành công!',
            'updated'   => 'Người dùng :name đã được cập nhật thành công!',
            'deleted'   => 'Người dùng :name đã được xóa thành công!'
        ]
    ],
    'kpi'   =>  [
        'success'   =>  [
            'created'   => 'KPI :name đã được thêm thành công!',
            'updated'   => 'KPI :name đã được cập nhật thành công!',
            'deleted'   => 'KPI :name đã được xóa thành công!',
            'initted'   => 'KPI :name đã được khởi tạo thành công!!'
        ]
    ],
    'error'   =>  [
        'notfound'      =>  'Không tìm thấy!',
        'creating'      =>  'Có lỗi trong quá trình thực hiện!',
        'updating'      =>  'Có lỗi trong quá trình thực hiện cập nhật!',
        'initting'      =>  'Có lỗi trong quá trình thực hiện khởi tạo!',
        'session'       =>  'Phiên đăng nhập của bạn đã hết vui lòng tải lại trang!'
    ]
];
