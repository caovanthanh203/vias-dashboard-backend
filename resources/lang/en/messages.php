<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'company'   =>  [
        'success'   =>  [
            'created'   => 'Company with name :name created successfull!',
            'updated'   => 'Company with name :name updated successfull!',
            'deleted'   => 'Company with name :name deleted successfull!'
        ]
    ],
    'organization'   =>  [
        'success'   =>  [
            'created'   => 'User with name :name created successfull!',
            'updated'   => 'User with name :name updated successfull!',
            'deleted'   => 'User with name :name deleted successfull!'
        ]
    ],
    'kpi'   =>  [
        'success'   =>  [
            'created'   => 'KPI with name :name created successfull!',
            'updated'   => 'KPI with name :name updated successfull!',
            'deleted'   => 'KPI with name :name deleted successfull!',
            'initted'   => 'KPI with name :name initted successfull!'
        ]
    ],
    'error'   =>  [
        'notfound'   =>  'Object not found!',
        'creating'   =>  'Something when wrong when processing!',
        'updating'   =>  'Something when wrong when updating!',
        'initting'   =>  'Something when wrong when initting!'
    ]
];
