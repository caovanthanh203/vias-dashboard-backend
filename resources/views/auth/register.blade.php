<!DOCTYPE html>
@if(session('key'))
    <html lang="{{App::getLocale()}}">
@else
    <html lang="vi">
@endif

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IRace</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    
    <script src="{{asset('js/admin/modernizr.min.js')}}"></script>
</head>


<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class=" card-box">
            <div class="panel-heading">
                <h4 class="text-center"> {{__('irace.login_page.sign_up_to')}}
                    <strong class="text-custom">IRACE</strong>
                </h4>
            </div>

            <div class="p-20">
                <form class="form-horizontal m-t-20" action="{{route('register')}}" method="post">
                    {{ csrf_field() }}
                    @if(count($errors)>0)
                    <div class="alert alert-danger{{(count($errors) > 0) ? '':' display-hide'}}"
                     style="    background-color:#fff;border-color: #fff;">
                        <button class="close" data-close="alert"></button>
                        <span>{{$errors->first('email')}}</span><br>
                        <span>{{$errors->first('password')}}</span>
                    </div>
                    @endif
                    <div class="form-group ">
                        <div class="col-12">
                            <input class="form-control" name="name" type="text" required="" placeholder="{{__('irace.login_page.name')}}" value="{{old('name')}}">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-12">
                            <input class="form-control" name="email" type="email" required="" placeholder="Email" value="{{old('email')}}">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-12">
                            <input class="form-control" name="confirm_email" type="email" required="" placeholder="{{__('irace.login_page.confirm_email')}}" value="{{old('email')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-12">
                            <input class="form-control" name="password" type="password" required="" placeholder="{{__('irace.login_page.pass')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-12">
                            <input class="form-control" name="confirm_password" type="password" required="" placeholder="{{__('irace.login_page.confirm_pass')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox" checked="checked" required>
                                <label for="checkbox-signup">{{__('irace.login_page.you_agree')}}
                                    <a href="#">{{__('irace.login_page.our_terms')}}</a>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center m-t-40">
                        <div class="col-12">
                            <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
                                {{__('irace.login_page.register')}}
                            </button>
                        </div>
                    </div>

                    <div class="form-group m-t-20 m-b-0">
                        <div class="col-12 text-center">
                            <h5 class="font-18">
                                <b>{{__('irace.login_page.sign_in_with')}}</b>
                            </h5>
                        </div>
                    </div>

                    <div class="form-group m-b-0 text-center">
                        <div class="col-12">
                            <a href="redirect">
                                <button type="button" class="btn btn-sm btn-facebook waves-effect waves-light m-t-20">
                                    <i class="fa fa-facebook m-r-5"></i> Facebook
                                </button>
                            </a>
                            <a href="redirectgoogle">
                                <button type="button" class="btn btn-sm btn-googleplus waves-effect waves-light m-t-20">
                                    <i class="fa fa-google-plus m-r-5"></i> Google+
                                </button>
                            </a>
                        </div>
                    </div>


                </form>

            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <p>
                    {{__('irace.login_page.how_1')}}
                    <a href="{{route("login")}}" class="text-primary m-l-5">
                        <b>{{__('irace.login_page.login')}}</b>
                    </a>
                </p>
            </div>
        </div>

    </div>

    <script>
        var resizefunc = [];
    </script>

     <script src="{{asset('js/admin/jquery.min.js')}}"></script>
    <script src="{{asset('js/admin/popper.min.js')}}"></script>
    <script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/detect.js')}}"></script>
    <script src="{{asset('js/admin/fastclick.js')}}"></script>
    <script src="{{asset('js/admin/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('js/admin/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/admin/waves.js')}}"></script>
    <script src="{{asset('js/admin/wow.min.js')}}"></script>
    <script src="{{asset('js/admin/jquery.nicescroll.js')}}"></script>
    <script src="{{asset('js/admin/jquery.scrollTo.min.js')}}"></script>

    <script src="{{asset('js/admin/jquery.core.js')}}"></script>
    <script src="{{asset('js/admin/jquery.app.js')}}"></script>

    <script src="{{asset('js/admin/vias-js.js')}}"></script>

</body>

</html>