<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    
    <script src="{{asset('js/admin/modernizr.min.js')}}"></script>
</head>
<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class=" card-box">
            <div class="panel-heading">
                <h4 class="text-center"> Khôi phục mật khẩu </h4>
            </div>
            <div class="p-20">
                <form method="post" action="{{ route('admin.password.email') }}" class="login-content-bg">
                    {{ csrf_field() }}
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            ×
                        </button>
                        Nhập
                        <b>Email</b> của bạn và chúng tôi sẽ gửi thông tin khôi phục cho bạn
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        @if ($errors->has('email'))
                        <div class="alert alert-danger">
                            {{ $errors->first('email') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group m-b-0">
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Nhập email" required="" name="email">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-pink w-sm waves-effect waves-light">
                                    Khôi phục
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        var resizefunc = [];
    </script>

<script src="{{asset('js/admin/jquery.min.js')}}"></script>
    <script src="{{asset('js/admin/popper.min.js')}}"></script>
    <script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/detect.js')}}"></script>
    <script src="{{asset('js/admin/fastclick.js')}}"></script>
    <script src="{{asset('js/admin/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('js/admin/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/admin/waves.js')}}"></script>
    <script src="{{asset('js/admin/wow.min.js')}}"></script>
    <script src="{{asset('js/admin/jquery.nicescroll.js')}}"></script>
    <script src="{{asset('js/admin/jquery.scrollTo.min.js')}}"></script>

    <script src="{{asset('js/admin/jquery.core.js')}}"></script>
    <script src="{{asset('js/admin/jquery.app.js')}}"></script>
</body>

</html>
