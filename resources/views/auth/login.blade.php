<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    
    <script src="{{asset('js/admin/modernizr.min.js')}}"></script>
</head>

<body>

    <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading">
                    <h4 class="text-center"> Đăng nhập vào <strong class="text-custom">Dashboard</strong></h4>
                </div>


                <div class="p-20">
                    <form class="form-horizontal m-t-20" method="post" action="{{route('admin.login')}}">
                        {{ csrf_field() }}
                        @if(count($errors)>0)
                        <div class="alert alert-danger{{(count($errors) > 0) ? '':' display-hide'}}"
                         style="    background-color:#fff;border-color: #fff;">
                            <button class="close" data-close="alert"></button>
                            <span>{{$errors->first('code')}}</span><br>
                            <span>{{$errors->first('password')}}</span>
                        </div>
                        @endif
                        <div class="form-group ">
                            <div class="col-12">
                                <input class="form-control" name="code" type="text" required="" placeholder="Username">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control" name="password" type="password" required="" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-12">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox-signup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}} >
                                    <label for="checkbox-signup">
                                        Ghi nhớ
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group text-center m-t-40">
                            <div class="col-12">
                                <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light"
                                        type="submit">Đăng nhập
                                </button>
                            </div>
                        </div>

                        <!-- <div class="form-group m-t-30 m-b-0">
                            <div class="col-12">
                                <a href="/reset" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot
                                    your password?</a>
                            </div>
                        </div> -->
                    </form>

                </div>
            </div>
            <!-- <div class="row">
                <div class="col-sm-12 text-center">
                    <p>Don't have an account? <a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a>
                    </p>

                </div>
            </div> -->
            
        </div>


    <script>
        var resizefunc = [];
    </script>

    <script src="{{asset('js/admin/jquery.min.js')}}"></script>
    <script src="{{asset('js/admin/popper.min.js')}}"></script>
    <script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/detect.js')}}"></script>
    <script src="{{asset('js/admin/fastclick.js')}}"></script>
    <script src="{{asset('js/admin/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('js/admin/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/admin/waves.js')}}"></script>
    <script src="{{asset('js/admin/wow.min.js')}}"></script>
    <script src="{{asset('js/admin/jquery.nicescroll.js')}}"></script>
    <script src="{{asset('js/admin/jquery.scrollTo.min.js')}}"></script>

    <script src="{{asset('js/admin/jquery.core.js')}}"></script>
    <script src="{{asset('js/admin/jquery.app.js')}}"></script>

    
</body>

</html>