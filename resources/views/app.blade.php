<!DOCTYPE html>
<html>
@include("include.header")
<body class="fixed-left">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="app"></div>
</body>
<script>
	var resizefunc = [];
</script>
@include("include.footer")
</html>
