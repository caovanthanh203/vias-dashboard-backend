<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta name="url" content="{{url("/")}}">
	<title>web-dashboard</title>
	<link rel="stylesheet" href="{{asset('css/spinners.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}">
	<link rel="stylesheet" href="{{asset('css/custom-for-dashboard.css')}}">
	<script src="{{asset('static/modernizr.min.js')}}"></script>
	@if(auth()->check())
	<meta name="role" content="{{auth()->user()->role_slug->slug}}">
	<meta name="role_name" content="{{auth()->user()->role_slug->slug_format}}">
  	<meta name="name" content="{{auth()->user()->short_name}}">
  	@else
	<meta name="role" content="guest">
	@endif
</head>